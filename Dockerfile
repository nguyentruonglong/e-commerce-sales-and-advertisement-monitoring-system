# Use an official Python runtime as a parent image
FROM python:3.9

# Set environment variables
ENV PYTHONUNBUFFERED 1
# ENV DJANGO_SETTINGS_MODULE ecommerce_monitoring_system.settings_production

# Set the working directory in the container
WORKDIR /code

# Copy the requirements file to the container and install dependencies
COPY requirements.txt /code/
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# Copy your source code into the container
COPY . /code/

# Expose the Gunicorn port
# EXPOSE 80

# Define the Gunicorn CMD
# CMD ["gunicorn", "--bind", "0.0.0.0:80", "--workers", "64", "--threads", "2", "--timeout", "1000", "--pythonpath", "/code/src_api", "ecommerce_monitoring_system.wsgi:application"]
