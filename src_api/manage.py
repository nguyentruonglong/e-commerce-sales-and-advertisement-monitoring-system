#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from django.views.decorators.debug import sensitive_variables

@sensitive_variables()
def main():
    # Get the value of DJANGO_SETTINGS_MODULE from environment variables
    settings_module = os.environ.get('DJANGO_SETTINGS_MODULE')

    if settings_module:
        # The value of DJANGO_SETTINGS_MODULE is set, use it
        os.environ['DJANGO_SETTINGS_MODULE'] = settings_module
    else:
        # The value is not set, provide a default value here
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ecommerce_monitoring_system.settings_local')

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
