from django.urls import include, path
from rest_framework import routers
from django.urls import include, re_path


# Define the URL patterns for your Django application.
urlpatterns = [
    # Include Shopify products analyzer URLs under the root path.
    path('', include('versioned.v1.shopify_products_analyzer.urls')),

    # Include Facebook ads analyzer URLs under the root path.
    path('', include('versioned.v1.facebook_ads_analyzer.urls')),

    # Include Facebook posts analyzer URLs under the root path.
    path('', include('versioned.v1.facebook_posts_analyzer.urls')),

    # Include data manager URLs under the root path.
    path('', include('versioned.v1.data_manager.urls')),
]
