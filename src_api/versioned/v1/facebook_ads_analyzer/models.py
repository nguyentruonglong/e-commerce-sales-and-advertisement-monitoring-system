from django.db import models
from django.utils import timezone

# Define the models for Facebook Ads Analyzer

# Facebook Page model
class FacebookPage(models.Model):
    # Primary key for the page
    page_id = models.CharField(max_length=100, primary_key=True)
    # URL of the Facebook page
    page_url = models.URLField(max_length=255)
    # Status of the page (True if working, False if not)
    page_status = models.BooleanField(default=True)
    # Name of the Facebook page
    page_name = models.CharField(max_length=1000, default='Unknown Name')
    # Timestamp for the last crawling
    last_crawling = models.DateTimeField(default=timezone.now)

    class Meta:
        # Ordering of records based on page URL
        ordering = ['page_url']

# Facebook Ad model
class FacebookAd(models.Model):
    # Foreign key to the Facebook Page (page_id is the referenced field)
    page_id = models.ForeignKey(FacebookPage, to_field='page_id', db_column='page_id',
                                related_query_name='facebook_ad', on_delete=models.CASCADE)
    # Primary key for the advertising
    advertising_id = models.CharField(max_length=100, primary_key=True)
    # URL of the embedded video
    embedded_video_url = models.URLField(max_length=1000)
    # URL of the embedded video's thumbnail
    embedded_video_thumbnail_url = models.URLField(max_length=1000)
    # Timestamp for when the ad started running
    started_running = models.DateTimeField(default=timezone.now)
    # URL of the product being advertised
    product_url = models.URLField(max_length=1000)
    # Price of the product
    price = models.FloatField(default=0.0)
    # Timestamp for when the record was created
    created_at = models.DateTimeField(default=timezone.now)
    # Timestamp for the last update
    updated_at = models.DateTimeField(default=timezone.now)
    # Flag to indicate if the ad is saved
    saved = models.BooleanField(default=False)
    # Timestamp for the last time the ad was saved
    last_saved = models.DateTimeField(default=timezone.now)
    # Description of the ad (optional)
    description = models.CharField(max_length=10000, blank=True)
