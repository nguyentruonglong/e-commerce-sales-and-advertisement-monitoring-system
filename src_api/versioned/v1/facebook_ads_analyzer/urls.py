from django.urls import path
from django.urls import include, re_path
from . import views as facebook_ad_views


# Define the URL patterns for your Django application
urlpatterns = [
    # Custom API routes for Facebook data
    re_path(r'facebook-pages',
            facebook_ad_views.FacebookPageViewSet.as_view({'get': 'list'})),
    re_path(r'facebook-ads', facebook_ad_views.FacebookAdViewSet.as_view(
        {'get': 'list', 'post': 'create', 'patch': 'partial_update'})),
]
