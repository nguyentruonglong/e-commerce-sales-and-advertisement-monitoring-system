import pytz
import dateutil

from datetime import datetime, timedelta
from django.db.models import (Case, Count, ExpressionWrapper, F, IntegerField, Max, Q, Subquery, Value, When)
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from rest_framework import filters, generics, permissions, status, viewsets
from rest_framework.pagination import DjangoPaginator, PageNumberPagination
from rest_framework.response import Response

from .models import FacebookPage, FacebookAd
from .serializers import FacebookPageSerializer, FacebookAdSerializer


class FacebookPageViewSet(viewsets.ModelViewSet):
    def __init__(self):
        # Set the serializer class for the viewset
        self.serializer_class = FacebookPageSerializer
        # Query all FacebookPage objects from the database
        self.queryset = FacebookPage.objects.all()

    # List view for handling GET requests to retrieve a list of Facebook Pages
    # @method_decorator(cache_page(60*30))  # Cache the response for 30 minutes
    def list(self, request):
        try:
            # Serialize the queryset data into a JSON response
            serializer = FacebookPageSerializer(
                instance=self.queryset, many=True)
            data_response = Response(serializer.data)
        except Exception as e:
            # Handle exceptions and return an error response
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            # Return the data response
            return data_response


class FacebookAdViewSet(viewsets.ModelViewSet):
    def __init__(self):
        # Get the current timestamp using the server's timezone
        self.timestamp = timezone.localtime(timezone.now())
        # Set the serializer class for the viewset
        self.serializer_class = FacebookAdSerializer

        # Display all active Facebook Ads for Facebook Pages
        self.queryset = FacebookAd.objects.annotate(
            page_url=F('page_id__page_url'),
            page_name=F('page_id__page_name'),
            last_crawling=F('page_id__last_crawling'),
            page_status=F('page_id__page_status')
        ).filter(page_status=True)

        # Define the initial query parameters
        self.query_params = {
            'tab': 'all-ads',
            'period': 'all-time',
            'sort_by': 'started-running',
            'facebook_page_id': str(),
        }

    def get_queryset(self):
        return self.queryset

    @method_decorator(cache_page(60))
    def list(self, request, *args, **kwargs):
        try:
            # Initialize variables
            period = 0
            self.pagination_class = PageNumberPagination

            # Update query_params based on request query parameters
            for key in request.query_params:
                if key in self.query_params:
                    self.query_params[key] = request.query_params[key]

            # Calculate the period based on 'period' query parameter
            valid_periods = ['1-day', '3-days', '7-days',
                             '14-days', '30-days', '90-days']
            if self.query_params['period'] in valid_periods:
                period = int(self.query_params['period'].replace(
                    'days', '').replace('day', '').replace('-', ''))

            # Define sort order mapping based on tab query parameter
            sort_order_mapping = {
                'all-ads': {
                    'started-running': '-started_running',
                    'created-at': '-created_at',
                    'last-sale': '-updated_at',
                    'default': '-started_running',
                },
                'saved': {
                    'default': '-last_saved',
                },
            }

            # Determine the sort order based on tab query parameter
            tab = self.query_params['tab']
            sort_by = self.query_params['sort_by'] if 'sort_by' in self.query_params else 'default'
            sort_order = sort_order_mapping.get(
                tab, {}).get(sort_by, 'default')

            # Apply sorting and filtering based on the determined sort order and period
            if period != 0:
                filter_params = {}
                if sort_by == 'last-sale':
                    filter_params['started_running__gte'] = self.timestamp - timedelta(
                        days=period, hours=self.timestamp.hour, minutes=self.timestamp.minute, seconds=self.timestamp.second)
                elif sort_by == 'created-at':
                    filter_params['created_at__gte'] = self.timestamp - timedelta(
                        days=period, hours=self.timestamp.hour, minutes=self.timestamp.minute, seconds=self.timestamp.second)
                else:
                    filter_params['started_running__gte'] = self.timestamp - timedelta(
                        days=period, hours=self.timestamp.hour, minutes=self.timestamp.minute, seconds=self.timestamp.second)
                self.queryset = self.queryset.filter(**filter_params)

            # Apply the determined sort order
            self.queryset = self.queryset.order_by(sort_order, 'page_id')

            # Paginate the queryset and serialize the data
            page = self.paginate_queryset(self.queryset)
            serializer_context = {'request': request}
            serializer = self.serializer_class(
                page, context=serializer_context, many=True
            )

            data_response = self.get_paginated_response(serializer.data)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @method_decorator(cache_page(60))
    # @transaction.atomic
    def create(self, request, *args, **kwargs):
        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)

            # Check if the required keys are not present in the request data
            if not all(key in request.data for key in ['facebook_page_url', 'page_status']):
                return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

            requested_page_url = request.data['facebook_page_url']
            requested_page_status = request.data['page_status']

            # If Facebook Page is inactive
            if not requested_page_status:
                FacebookPage.objects.filter(page_url=requested_page_url).update(
                    page_status=requested_page_status)
                return data_response

            # If Facebook Page is active and contains additional data
            if all(key in request.data for key in ['facebook_page_id', 'facebook_ads_info', 'facebook_page_name', 'last_crawling']):
                requested_page_id = request.data['facebook_page_id']
                requested_page_name = request.data['facebook_page_name']
                last_crawling = dateutil.parser.parse(
                    request.data['last_crawling']).astimezone(pytz.timezone('Etc/UTC'))
                requested_ads_info = request.data['facebook_ads_info']

                # Check if the Facebook Page with the same ID and URL doesn't exist, delete others
                if FacebookPage.objects.filter(page_id=requested_page_id, page_url=requested_page_url).count() == 0:
                    FacebookPage.objects.filter(
                        ~Q(page_id=requested_page_id), page_url=requested_page_url).delete()

                # Update or create the Facebook Page
                FacebookPage.objects.update_or_create(
                    page_id=requested_page_id,
                    defaults={
                        'page_url': requested_page_url,
                        'page_name': requested_page_name,
                        'last_crawling': last_crawling,
                        'page_status': requested_page_status,
                    }
                )

                input_ad_ids = list(requested_ads_info.keys())
                input_ad_ids.sort()

                # Fetch the advertising IDs currently associated with the Facebook Page
                filtered_ad_objs = FacebookAd.objects.filter(page_id=FacebookPage.objects.get(
                    page_id=requested_page_id)).values('advertising_id')
                current_ad_ids = list(
                    map(lambda e: e['advertising_id'], filtered_ad_objs))

                # Determine the IDs of ads that need to be deleted
                deleted_ad_ids = [
                    ad_id for ad_id in current_ad_ids if ad_id not in input_ad_ids]

                # Create new advertising objects for ads that are not in the current set
                created_ad_objects = [requested_ads_info[ad_id]
                                      for ad_id in input_ad_ids if ad_id not in current_ad_ids]

                # Update existing advertising objects
                updated_ad_objects = [requested_ads_info[ad_id]
                                      for ad_id in input_ad_ids if ad_id in current_ad_ids]

                # Delete the advertising objects marked for deletion
                FacebookAd.objects.filter(
                    advertising_id__in=deleted_ad_ids).delete()

                # Bulk create new advertising objects
                FacebookAd.objects.bulk_create([
                    FacebookAd(
                        advertising_id=ad_obj['advertising_id'],
                        started_running=dateutil.parser.parse(
                            ad_obj['started_running']).astimezone(pytz.timezone('Etc/UTC')),
                        price=float(ad_obj['price']),
                        created_at=dateutil.parser.parse(
                            ad_obj['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                        updated_at=dateutil.parser.parse(
                            ad_obj['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                        embedded_video_url=ad_obj['embedded_video_url'],
                        embedded_video_thumbnail_url=ad_obj['video_thumbnail_url'],
                        product_url=ad_obj['advertised_product_url'],
                        page_id=FacebookPage.objects.get(
                            page_id=requested_page_id),
                    ) for ad_obj in created_ad_objects]
                )

                # Bulk update existing advertising objects
                updated_ad_queryset = [FacebookAd.objects.get(
                    advertising_id=ad_obj['advertising_id']) for ad_obj in updated_ad_objects]
                for i_th in range(len(updated_ad_objects)):
                    updated_ad_queryset[i_th].started_running = dateutil.parser.parse(
                        updated_ad_objects[i_th]['started_running']).astimezone(pytz.timezone('Etc/UTC'))
                    updated_ad_queryset[i_th].price = float(
                        updated_ad_objects[i_th]['price'])
                    updated_ad_queryset[i_th].created_at = dateutil.parser.parse(
                        updated_ad_objects[i_th]['created_at']).astimezone(pytz.timezone('Etc/UTC'))
                    updated_ad_queryset[i_th].updated_at = dateutil.parser.parse(
                        updated_ad_objects[i_th]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))
                    updated_ad_queryset[i_th].embedded_video_url = updated_ad_objects[i_th]['embedded_video_url']
                    updated_ad_queryset[i_th].embedded_video_thumbnail_url = updated_ad_objects[i_th]['video_thumbnail_url']
                    updated_ad_queryset[i_th].product_url = updated_ad_objects[i_th]['advertised_product_url']
                    updated_ad_queryset[i_th].description = str()

                FacebookAd.objects.bulk_update(
                    updated_ad_queryset,
                    ['started_running', 'price', 'created_at', 'updated_at', 'embedded_video_url', 'embedded_video_thumbnail_url', 'product_url', 'description'])

            else:
                data_response = JsonResponse(
                    {'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print(str(e))
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    # @transaction.atomic
    @csrf_exempt
    def partial_update(self, request, *args, **kwargs):
        try:
            # Check if the required keys are present in the request data
            if not all(key in request.data for key in ['advertising_id', 'last_saved']):
                return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

            advertising_id = request.data['advertising_id']
            last_saved_time_string = request.data['last_saved']
            last_saved_time_object = dateutil.parser.parse(
                last_saved_time_string)

            # Update the FacebookAd with advertising_id, set 'saved' to the opposite of its current value, and update 'last_saved'
            FacebookAd.objects.filter(advertising_id=advertising_id).update(
                saved=Case(
                    When(saved=True, then=False),
                    default=True
                ),
                last_saved=last_saved_time_object
            )

            data_response = JsonResponse({}, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response
