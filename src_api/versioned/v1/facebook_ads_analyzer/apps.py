from django.apps import AppConfig


class FacebookAdsAnalyzerConfig(AppConfig):
    name = 'versioned.v1.facebook_ads_analyzer'
    label = 'facebook_ads_analyzer_v1'
