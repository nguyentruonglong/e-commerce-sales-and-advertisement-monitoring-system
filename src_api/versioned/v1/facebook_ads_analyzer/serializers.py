from . models import FacebookPage, FacebookAd
from rest_framework import serializers


# Serializer for the FacebookAd model
class FacebookAdSerializer(serializers.ModelSerializer):
    # Define page_url as a URL field
    page_url = serializers.URLField()
    # Define page_name as a character field
    page_name = serializers.CharField()
    # Define last_crawling as a DateTime field
    last_crawling = serializers.DateTimeField()

    class Meta:
        # Set the model for the serializer to FacebookAd
        model = FacebookAd
        # Include all fields from the model in the serialization
        fields = '__all__'


# Serializer for the FacebookPage model
class FacebookPageSerializer(serializers.ModelSerializer):
    class Meta:
        # Set the model for the serializer to FacebookPage
        model = FacebookPage
        # Define the ordering of results based on page_url
        ordering = ['page_url']
        # Include all fields from the model in the serialization
        fields = '__all__'
