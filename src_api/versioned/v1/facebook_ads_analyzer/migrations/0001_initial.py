# Generated by Django 3.1 on 2021-08-23 16:52

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FacebookPage',
            fields=[
                ('page_id', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('page_url', models.URLField(max_length=255)),
                ('page_status', models.BooleanField(default=True)),
                ('page_name', models.CharField(default='Unknown Name', max_length=1000)),
                ('last_crawling', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'ordering': ['page_url'],
            },
        ),
        migrations.CreateModel(
            name='FacebookAd',
            fields=[
                ('advertising_id', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('embedded_video_url', models.URLField(max_length=1000)),
                ('embedded_video_thumbnail_url', models.URLField(max_length=1000)),
                ('started_running', models.DateTimeField(default=django.utils.timezone.now)),
                ('product_url', models.URLField(max_length=1000)),
                ('price', models.FloatField(default=0.0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('saved', models.BooleanField(default=False)),
                ('last_saved', models.DateTimeField(default=django.utils.timezone.now)),
                ('description', models.CharField(blank=True, max_length=10000)),
                ('page_id', models.ForeignKey(db_column='page_id', on_delete=django.db.models.deletion.CASCADE, related_query_name='facebook_ad', to='facebook_ads_analyzer_v1.facebookpage')),
            ],
        ),
    ]
