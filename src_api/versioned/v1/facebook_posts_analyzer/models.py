from django.db import models
from django.utils import timezone
import uuid

# Create your models here.


class FacebookVideoPost(models.Model):
    # Unique identifier for the post
    post_id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True)
    # A unique number assigned to the post
    post_number = models.CharField(
        max_length=100, default=uuid.uuid4, unique=True)
    # URL of the Facebook post
    post_url = models.URLField(max_length=255)
    # Status of the Facebook post (active or inactive)
    post_status = models.BooleanField(default=False)
    # Timestamp when the post was created
    posted_at = models.DateTimeField(
        default=timezone.now)
    # URL of the embedded video (if applicable)
    embedded_video_url = models.URLField(max_length=1000, blank=True)
    # URL of the video's thumbnail (if applicable)
    video_thumbnail_url = models.URLField(max_length=1000, blank=True)
    # URL of the photo's thumbnail (if applicable)
    photo_thumbnail_url = models.URLField(max_length=1000, blank=True)
    # URL of the post's thumbnail (if applicable)
    post_thumbnail_url = models.URLField(max_length=1000, blank=True)
    # Title of the video (if applicable)
    video_title = models.CharField(max_length=200, default='Unknown Title')
    # Trending index of the post
    trending_index = models.IntegerField(default=0)
    # Timestamp when the post was last crawled
    last_crawling = models.DateTimeField(default=timezone.now)
    # URL of the associated Facebook page (if applicable)
    facebook_page_url = models.CharField(max_length=200, blank=True)
    # Name of the associated Facebook page
    facebook_page_name = models.CharField(
        max_length=200, default='Unknown Name')
    # Flag indicating if the post is saved
    saved = models.BooleanField(default=False)
    # Timestamp when the post was last saved
    last_saved = models.DateTimeField(default=timezone.now)
    # Category of the post
    category = models.CharField(
        max_length=200, default='Unknown Category', blank=True)
    # Description of the post
    description = models.CharField(max_length=10000, blank=True)
    # Delta values for reaction count, share count, and comment count
    reaction_count_delta = models.IntegerField(default=0)
    share_count_delta = models.IntegerField(default=0)
    comment_count_delta = models.IntegerField(default=0)
    # Daily engagement chart in JSON format
    daily_engagement_chart = models.CharField(
        max_length=1000, default='{"daily_reaction_count": {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0}, "daily_share_count": {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0}, "daily_comment_count": {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0}}')

    class Meta:
        # Default ordering by post URL
        ordering = ['post_url']


class Engagement(models.Model):
    # Unique identifier for the engagement
    engagement_id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True)
    # Reference to the associated FacebookVideoPost
    post_id = models.ForeignKey(FacebookVideoPost, to_field='post_id', db_column='post_id',
                                related_name='engagements', related_query_name='engagement', on_delete=models.CASCADE)
    # Timestamp when the engagement data was crawled
    crawled_at = models.DateTimeField(
        default=timezone.now)
    # Count of reactions
    reaction_count = models.IntegerField(default=0)
    # Count of shares
    share_count = models.IntegerField(default=0)
    # Count of comments
    comment_count = models.IntegerField(default=0)
