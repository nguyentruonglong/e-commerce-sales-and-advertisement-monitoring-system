from django.urls import include, re_path
from . import views as facebook_post_views


# Define the URL patterns for your Django application
urlpatterns = [
    # Custom API route for video post
    re_path(r'facebook-video-posts', facebook_post_views.FacebookVideoPostViewSet.as_view(
            {'get': 'list', 'post': 'create'})),
    re_path(r'video-posts-analytics', facebook_post_views.VideoPostAnalyticsViewSet.as_view(
        {'get': 'list', 'patch': 'partial_update'})),

    # Custom API route for deleting video post analytics by post ID
    re_path(r'video-posts-analytics/(?P<post_id>([0-9]{5,40}))/$', facebook_post_views.VideoPostAnalyticsViewSet.as_view(
        {'delete': 'destroy'})),
]
