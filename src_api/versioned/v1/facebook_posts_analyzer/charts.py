from datetime import datetime, timedelta
from django.utils import timezone
from django.db.models import Count, F, Q, Max
from versioned.v1.facebook_posts_analyzer.models import FacebookVideoPost, Engagement


def get_start_of_day(x_max, date, timestamp):
    # Calculate the start time of a given day
    start_time = timestamp - timedelta(days=x_max - date, hours=timestamp.hour,
                                       minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond)
    return start_time


def get_end_of_day(x_max, date, timestamp):
    # Calculate the end time of a given day
    end_time = timestamp if timestamp < timestamp - timedelta(days=x_max - date - 1, hours=timestamp.hour,
                                                              minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond) else timestamp - timedelta(days=x_max - date - 1, hours=timestamp.hour, minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond)
    return end_time


def get_cartesian_coordinates(post_id, timestamp, engagement_type):
    try:
        x_min = 1
        x_max = 7
        cartesian_coordinates = dict()
        dates = list(range(x_min, x_max + 1))

        engagement_queryset = Engagement.objects.filter(
            post_id=FacebookVideoPost.objects.get(post_id__in=[post_id])).values('post_id').annotate(
            **{
                f'day_{date}': Max(engagement_type, filter=Q(
                    crawled_at__range=(get_start_of_day(x_max, date, timestamp),
                                       get_end_of_day(x_max, date, timestamp))
                ))
                for date in dates
            }
        )

        cartesian_coordinates_result = {str(date): 0 for date in dates}
        for date in dates:
            tmp_cartesian_coordinates = engagement_queryset[0][f'day_{date}']
            if engagement_type != 'share_count' and tmp_cartesian_coordinates is None:
                tmp_cartesian_coordinates = 0
            cartesian_coordinates_result[str(date)] = tmp_cartesian_coordinates

    except Exception as e:
        print(e)
        cartesian_coordinates_result = {str(date): 0 for date in dates}
    finally:
        return cartesian_coordinates_result


def calculate_daily_engagement_chart(post_id, timestamp):
    return {
        'daily_reaction_count': get_cartesian_coordinates(post_id, timestamp, 'reaction_count'),
        'daily_share_count': get_cartesian_coordinates(post_id, timestamp, 'share_count'),
        'daily_comment_count': get_cartesian_coordinates(post_id, timestamp, 'comment_count'),
    }
