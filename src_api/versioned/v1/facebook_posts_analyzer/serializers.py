from .models import FacebookVideoPost, Engagement
from rest_framework import serializers


class FacebookVideoPostSerializer(serializers.ModelSerializer):
    # Number of reactions for the post on the latest day
    latest_reaction_count = serializers.IntegerField(
        initial=0, default=0, allow_null=False)
    # Number of shares for the post on the latest day
    latest_share_count = serializers.IntegerField(
        initial=0, default=0, allow_null=False)
    # Number of comments for the post on the latest day
    latest_comment_count = serializers.IntegerField(
        initial=0, default=0, allow_null=False)

    class Meta:
        model = FacebookVideoPost
        ordering = ['post_url']
        fields = '__all__'

    # Override the to_representation() method of FacebookVideoPostSerializer
    def to_representation(self, instance):
        # Call the to_representation() method of the parent class
        data = super().to_representation(instance)
        # Check the values of these fields one by one and replace them with 0 if they are None
        if data['latest_reaction_count'] is None:
            data['latest_reaction_count'] = 0
        if data['latest_share_count'] is None:
            data['latest_share_count'] = 0
        if data['latest_comment_count'] is None:
            data['latest_comment_count'] = 0
        return data


class PartialVideoPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = FacebookVideoPost
        fields = ('post_id', 'post_number', 'post_url',
                  'posted_at', 'post_status')


class EngagementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Engagement
        fields = '__all__'
