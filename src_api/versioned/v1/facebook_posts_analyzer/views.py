import json
import dateutil
import pytz

from django.db import transaction
from django.db.models import (
    Case, Count, ExpressionWrapper, F, IntegerField, Max, Q, Subquery, Value, When, Sum, OuterRef
)
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta

from rest_framework import filters, generics, permissions, status, viewsets
from rest_framework.decorators import action, authentication_classes, parser_classes, permission_classes
from rest_framework.parsers import FileUploadParser, JSONParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from versioned.v1.facebook_posts_analyzer import charts
from versioned.v1.facebook_posts_analyzer.models import FacebookVideoPost, Engagement
from .serializers import FacebookVideoPostSerializer, PartialVideoPostSerializer


def get_starttime_of_day(dates, timestamp):
    return (timestamp - timedelta(days=dates, hours=timestamp.hour,
                                  minutes=timestamp.minute, seconds=timestamp.second,
                                  microseconds=timestamp.microsecond))


class FacebookVideoPostViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.serializer_class = PartialVideoPostSerializer
        self.timestamp = timezone.localtime(timezone.now())
        self.queryset = FacebookVideoPost.objects.all()

    def list(self, request):
        try:
            # Response with data containing objects with fields post id, post url, and posted at
            serializer = PartialVideoPostSerializer(
                instance=self.queryset, many=True)
            return Response(serializer.data)
        except Exception as e:
            print(e)
            return JsonResponse({'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)
            FacebookVideoPost.objects.filter(post_id=str()).delete()

            # Check for post data
            if not all(key in request.data for key in ['post_id', 'post_url', 'post_status']):
                return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

            requested_post_id = request.data['post_id']
            requested_post_url = request.data['post_url']
            requested_post_status = request.data['post_status']

            # Handle deleted or inaccessible posts
            if not requested_post_status:
                FacebookVideoPost.objects.filter(post_id=requested_post_id).update(
                    post_status=requested_post_status)
            else:
                if not all(key in request.data for key in
                           ['post_number', 'last_crawling', 'reaction_count', 'comment_count', 'share_count', 'posted_at']):
                    return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

                requested_post_number = request.data['post_number']
                if requested_post_number == str() or requested_post_number == None:
                    return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

                last_crawling = dateutil.parser.parse(
                    request.data['last_crawling']).astimezone(pytz.timezone('Etc/UTC'))

                # Check for duplicate post numbers
                if FacebookVideoPost.objects.filter(post_number=requested_post_number).count() > 1:
                    FacebookVideoPost.objects.filter(
                        post_id=requested_post_id).delete()
                else:
                    # Insert or update the record with the correct post number
                    FacebookVideoPost.objects.update_or_create(
                        post_id=requested_post_id,
                        defaults={
                            'post_url': requested_post_url,
                            'post_number': requested_post_number,
                            'post_status': True,
                            'last_crawling': last_crawling,
                            'video_thumbnail_url': request.data['video_thumbnail_url'],
                            'embedded_video_url': request.data['embedded_video_url'],
                            'facebook_page_url': request.data['facebook_page_url'],
                            'facebook_page_name': request.data['facebook_page_name'],
                            'video_title': request.data['video_title'],
                            'posted_at': dateutil.parser.parse(request.data['posted_at']).astimezone(pytz.timezone('Etc/UTC')),
                        }
                    )

                    # Create Engagement object
                    Engagement.objects.create(
                        reaction_count=int(request.data['reaction_count']),
                        comment_count=int(request.data['comment_count']),
                        share_count=int(request.data['share_count']),
                        crawled_at=timezone.localtime(timezone.now()),
                        post_id=FacebookVideoPost.objects.get(
                            post_id=requested_post_id)
                    )

                    # Calculate trending index
                    engagement_queryset = Engagement.objects.filter(post_id=requested_post_id).values('post_id').annotate(
                        trending_index=Sum('share_count', filter=Q(
                            crawled_at__gt=get_starttime_of_day(2, self.timestamp))),
                        reaction_count_in_yesterday=Max('reaction_count', filter=Q(crawled_at__gt=get_starttime_of_day(
                            1, self.timestamp), crawled_at__lt=get_starttime_of_day(0, self.timestamp))),
                        share_count_in_yesterday=Max('share_count', filter=Q(crawled_at__gt=get_starttime_of_day(
                            1, self.timestamp), crawled_at__lt=get_starttime_of_day(0, self.timestamp))),
                        comment_count_in_yesterday=Max('comment_count', filter=Q(crawled_at__gt=get_starttime_of_day(
                            1, self.timestamp), crawled_at__lt=get_starttime_of_day(0, self.timestamp)))
                    )[0]

                    # Calculate daily engagement chart
                    daily_engagement_chart_object = charts.calculate_daily_egagement_chart(
                        requested_post_id, self.timestamp)
                    daily_engagement_chart_object['daily_reaction_count']['7'] = int(
                        request.data['reaction_count'])
                    daily_engagement_chart_object['daily_share_count']['7'] = int(
                        request.data['share_count'])
                    daily_engagement_chart_object['daily_comment_count']['7'] = int(
                        request.data['comment_count'])

                    # Calculate trending index
                    starttime_share_count = 0
                    copied_daily_share_chart = dict.copy(
                        daily_engagement_chart_object['daily_share_count'])
                    daily_share_count_list = list(
                        daily_engagement_chart_object['daily_share_count'].values())

                    for share_count_element in daily_share_count_list:
                        if share_count_element != None:
                            starttime_share_count = share_count_element
                            break

                    for date, share_count in daily_engagement_chart_object['daily_share_count'].items():
                        is_firsttime = False
                        if share_count != None and not is_firsttime:
                            is_firsttime = True
                        elif share_count == None and not is_firsttime:
                            copied_daily_share_chart[date] = starttime_share_count
                            daily_engagement_chart_object['daily_share_count'][date] = 0
                        elif share_count == None and is_firsttime:
                            copied_daily_share_chart[date] = 0
                            daily_engagement_chart_object['daily_share_count'][date] = 0

                    trending_index = copied_daily_share_chart['7'] - \
                        copied_daily_share_chart['4']

                    # Update post data
                    FacebookVideoPost.objects.filter(post_id=requested_post_id).update(
                        trending_index=int(trending_index),
                        reaction_count_delta=int(request.data['reaction_count']) - (int(engagement_queryset['reaction_count_in_yesterday'])
                                                                                    if engagement_queryset['reaction_count_in_yesterday'] is not None else int(request.data['reaction_count'])),
                        share_count_delta=int(request.data['share_count']) - (int(engagement_queryset['share_count_in_yesterday'])
                                                                              if engagement_queryset['share_count_in_yesterday'] is not None else int(request.data['share_count'])),
                        comment_count_delta=int(request.data['comment_count']) - (int(engagement_queryset['comment_count_in_yesterday'])
                                                                                  if engagement_queryset['comment_count_in_yesterday'] is not None else int(request.data['comment_count'])),
                        daily_engagement_chart=json.dumps(
                            daily_engagement_chart_object),
                    )
        except Exception as e:
            print(str(e))
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class VideoPostAnalyticsViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.timestamp = timezone.localtime(timezone.now())
        self.serializer_class = FacebookVideoPostSerializer
        # Query the child model Engagement to get the latest reaction count
        # Query the child model Engagement to get the latest share count
        # Query the child model Engagement to get the latest comment count
        # Only display active posts
        self.queryset = FacebookVideoPost.objects.annotate(
            latest_reaction_count=Subquery(
                Engagement.objects.filter(
                    post_id=OuterRef('post_id')
                ).values('post_id').annotate(
                    latest_reaction_count=Max('reaction_count', filter=Q(
                        crawled_at__gt=get_starttime_of_day(1, self.timestamp)))
                ).values('latest_reaction_count')[:1]
            ),
            latest_share_count=Subquery(
                Engagement.objects.filter(
                    post_id=OuterRef('post_id')
                ).values('post_id').annotate(
                    latest_share_count=Max('share_count', filter=Q(
                        crawled_at__gt=get_starttime_of_day(1, self.timestamp)))
                ).values('latest_share_count')[:1]
            ),
            latest_comment_count=Subquery(
                Engagement.objects.filter(
                    post_id=OuterRef('post_id')
                ).values('post_id').annotate(
                    latest_comment_count=Max('comment_count', filter=Q(
                        crawled_at__gt=get_starttime_of_day(1, self.timestamp)))
                ).values('latest_comment_count')[:1]
            ),
        )
        # .filter(post_status=True)

        self.query_params = {
            'tab': 'all-posts',
            'period': 'all-time',
            'sort_by': 'posted-at',
            'post_id': str(),
        }

    def get_queryset(self):
        return self.queryset

    @cache_page(60 * 1)
    @transaction.atomic
    def list(self, request, *args, **kwargs):
        try:
            self.pagination_class = PageNumberPagination

            # Copy query parameters from the request
            for key in request.query_params:
                if key in self.query_params:
                    self.query_params[key] = request.query_params[key]

            # Extract and parse the period filter
            period = None
            if self.query_params['period'] in ['1-day', '3-days', '7-days', '14-days', '30-days', '90-days']:
                period = int(self.query_params['period'].replace(
                    'days', '').replace('day', '').replace('-', ''))

            if period is not None:
                # Filter queryset by the specified period
                self.queryset = self.queryset.filter(posted_at__gte=(self.timestamp - timedelta(
                    days=period, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                    seconds=self.timestamp.second)))

            # Filter by post_id if it's provided
            post_id = self.query_params['post_id']
            if post_id:
                self.queryset = self.queryset.filter(post_id=post_id)

            # Apply sorting based on the selected tab and sort_by
            if self.query_params['tab'] == 'all-posts':
                sorting_field = '-posted_at'  # Default sorting field
                if self.query_params['sort_by'] == 'trending':
                    sorting_field = '-trending_index'
                elif self.query_params['sort_by'] == 'sharing':
                    sorting_field = '-latest_share_count'

                # Apply sorting
                self.queryset = self.queryset.order_by(
                    sorting_field, '-posted_at')

            elif self.query_params['tab'] == 'saved':
                # Filter and sort for the 'saved' tab
                self.queryset = self.queryset.filter(
                    saved=True).order_by('-last_saved', 'post_id')

            # Paginate the queryset and serialize the data
            page = self.paginate_queryset(self.queryset)
            serializer_context = {'request': request}
            serializer = self.serializer_class(
                page, context=serializer_context, many=True)

            data_response = self.get_paginated_response(serializer.data)

        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @transaction.atomic
    @csrf_exempt
    def partial_update(self, request, *args, **kwargs):
        # Check if the required keys are present in the request data
        if not all(key in request.data for key in ['post_id', 'last_saved']):
            return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            post_id = request.data['post_id']
            last_saved_time_string = request.data['last_saved']
            last_saved_time_object = dateutil.parser.parse(
                last_saved_time_string)

            # Update the 'saved' status and 'last_saved' time for the specified post
            FacebookVideoPost.objects.filter(post_id=post_id).update(saved=Case(
                When(saved=True, then=False), default=True), last_saved=last_saved_time_object)

            data_response = JsonResponse({}, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @parser_classes([JSONParser])
    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)
            if request.GET.get('post_id') is None:
                data_response = HttpResponse(
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                deleted_post_id = request.GET.get('post_id')

                # Delete the specified post by its ID
                FacebookVideoPost.objects.filter(
                    post_id=deleted_post_id).delete()

        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response
