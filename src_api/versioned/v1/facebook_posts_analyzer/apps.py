from django.apps import AppConfig


class FacebookPostsAnalyzerConfig(AppConfig):
    name = 'versioned.v1.facebook_posts_analyzer'
    label = 'facebook_posts_analyzer_v1'