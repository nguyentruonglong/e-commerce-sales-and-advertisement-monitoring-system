from rest_framework import serializers


class DataUploadViewSerializer(serializers.Serializer):
    
    class Meta:
        # Set the reference name
        ref_name = 'v1'
