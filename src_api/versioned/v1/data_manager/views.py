import os
import re
import validators

from furl import furl

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.parsers import FileUploadParser, JSONParser, MultiPartParser

from .constants import SHOPIFY_STORE, SHOPBASE_STORE, FACEBOOK_PAGE, FACEBOOK_POST

from versioned.v1.facebook_ads_analyzer.models import FacebookPage
from versioned.v1.facebook_posts_analyzer.models import FacebookVideoPost
from versioned.v1.shopify_products_analyzer.models import ShopifyStore

from versioned.v1.data_manager.serializers import DataUploadViewSerializer


class DataUploadView(LoginRequiredMixin, CreateAPIView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'
    parser_classes = (MultiPartParser,)
    serializer_class = DataUploadViewSerializer


    # Read the content of a CSV file and extract relevant data into a list
    def _read_file_content_into_list(self, csv_file, data_type):
        filtered_data = []
        csv_file_contents = csv_file.read().decode('utf-8')
        input_data = csv_file_contents.replace('\r', '').strip().split('\n')
        input_data = list(set(input_data))

        for data in input_data:
            # Check if the data follows URL format using regular expression
            if not validators.re_path(data):
                matched_data = re.findall(
                    r'(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)', data)
                data = matched_data[0] if matched_data else ''

            if data:
                url_structure = furl(data.strip())
                if data_type == SHOPIFY_STORE or data_type == SHOPBASE_STORE:
                    data = url_structure.origin
                elif data_type == FACEBOOK_PAGE:
                    data = url_structure.origin + str(url_structure.path)
                elif data_type == FACEBOOK_POST:
                    data = data.strip()
                filtered_data.append(data)

        filtered_data = list(set(filtered_data))
        return filtered_data

    # Process the uploaded data file and add it to the corresponding database table
    def _process_data_file(self, request, field_name, data_type):
        try:
            csv_file = request.FILES.get(field_name)
            if not csv_file:
                return JsonResponse({'error': 'File not provided'}, status=status.HTTP_400_BAD_REQUEST)

            # Determine the data type and get the current data
            if data_type == SHOPIFY_STORE:
                current_data = list(
                    ShopifyStore.objects.values_list('store_url', flat=True))
            elif data_type == FACEBOOK_PAGE:
                current_data = list(
                    FacebookPage.objects.values_list('page_url', flat=True))
            elif data_type == FACEBOOK_POST:
                current_data = list(
                    FacebookVideoPost.objects.values_list('post_url', flat=True))
            else:
                return JsonResponse({'error': 'Invalid data type'}, status=status.HTTP_400_BAD_REQUEST)

            extension = os.path.splitext(csv_file.name)[1]
            if extension not in ('.txt', '.csv'):
                return JsonResponse({'error': 'Invalid file format'}, status=status.HTTP_400_BAD_REQUEST)

            filtered_data = self._read_file_content_into_list(
                csv_file, data_type)

            if data_type == SHOPIFY_STORE:
                inserted_data = [
                    data for data in filtered_data if data not in current_data]
                ShopifyStore.objects.bulk_create([ShopifyStore(
                    store_url=data, category=SHOPIFY_STORE, store_status=False) for data in inserted_data])
            elif data_type == FACEBOOK_PAGE:
                inserted_data = [
                    data for data in filtered_data if data not in current_data]
                FacebookPage.objects.bulk_create(
                    [FacebookPage(page_url=data) for data in inserted_data])
            elif data_type == FACEBOOK_POST:
                inserted_data = [
                    data for data in filtered_data if data not in current_data]
                FacebookVideoPost.objects.bulk_create(
                    [FacebookVideoPost(post_url=data) for data in inserted_data])

            return JsonResponse({}, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return JsonResponse({'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)

    @csrf_exempt
    # Handle POST requests
    def create(self, request, *args, **kwargs):
        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)
            data_fields = ['shopify_store_data_file', 'shopbase_store_data_file',
                           'facebook_page_data_file', 'facebook_post_data_file']
            data_types = [SHOPIFY_STORE, SHOPBASE_STORE,
                          FACEBOOK_PAGE, FACEBOOK_POST]

            for field_name, data_type in zip(data_fields, data_types):
                if field_name in request.FILES:
                    data_response = self._process_data_file(
                        request, field_name, data_type)
                    if data_response.status_code != status.HTTP_200_OK:
                        break

        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response
