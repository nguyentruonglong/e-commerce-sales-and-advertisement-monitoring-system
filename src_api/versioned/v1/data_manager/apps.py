from django.apps import AppConfig


class DataManagerConfig(AppConfig):
    name = 'versioned.v1.data_manager'
    label = 'data_manager_v1'
