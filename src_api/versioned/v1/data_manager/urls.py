from django.urls import path, re_path
from . import views


urlpatterns = [
    # Route for handling data uploads, protected from CSRF
    path(r'upload', views.DataUploadView.as_view()),
]
