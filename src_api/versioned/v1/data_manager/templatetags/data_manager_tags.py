from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
import hashlib

register = template.Library()


# Custom filter to extract a list of Facebook pages that are advertising for the given store.
@register.filter(name='filter_facebook_pages_for_store')
def filter_facebook_pages_for_store(facebook_page_objects, store_url):
    filtered_pages = []  # Initialize an empty list to store filtered pages
    for page_object in facebook_page_objects:
        # Check if the store URL is present in the product URL of the page_object
        if store_url in str(page_object['product_url']):
            # Append matching pages to the filtered list
            filtered_pages.append(page_object)
    return filtered_pages  # Return the filtered list


# Custom filter to compute the MD5 hash of an input string
@register.filter(name='md5')
def md5_string(input_value):
    # Calculate the MD5 hash of the input string and convert it to hexadecimal format
    return hashlib.md5(input_value.encode('utf-8')).hexdigest()
