from django.urls import path
from django.urls import include, re_path
from . import views as shopify_product_views


# Define the URL patterns for your Django application
urlpatterns = [
    # Define custom API routes using re_path for Shopify and ShopBase stores
    re_path(r'shopify-stores',
            shopify_product_views.ShopifyStoreViewSet.as_view({'get': 'list'})),
    re_path(r'shopbase-stores',
            shopify_product_views.ShopifyStoreViewSet.as_view({'get': 'list'})),

    # Define custom API routes for Shopify and ShopBase products
    re_path(r'shopify-products', shopify_product_views.ShopifyProductViewSet.as_view(
        {'get': 'list', 'post': 'create', 'patch': 'partial_update'})),
    re_path(r'shopbase-products', shopify_product_views.ShopifyProductViewSet.as_view(
        {'get': 'list', 'post': 'create', 'patch': 'partial_update'})),
    re_path(r'daily-sales-chart', shopify_product_views.DailySalesChartViewSet.as_view(
        {'get': 'list'})),
]
