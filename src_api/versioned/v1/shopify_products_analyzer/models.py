from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import JSONField
import uuid


class ShopifyStore(models.Model):
    # Primary key field for the store's URL
    store_url = models.URLField(max_length=255, primary_key=True)
    # Store's old domain
    old_domain = models.URLField(max_length=255, default=str())
    # Title of the store's website
    website_title = models.CharField(max_length=1000, default='Unknown Title')
    # Last time the store was crawled
    last_crawling = models.DateTimeField(default=timezone.now)
    # Currency code used by the store
    currency_code = models.CharField(max_length=10, default='USD')
    # Status of the store
    store_status = models.BooleanField(default=True)
    # Number of times the store was updated
    updated_number = models.IntegerField(default=0)
    # Category of the store
    category = models.CharField(max_length=100, default='Unknown Category')

    class Meta:
        # Define the default ordering for querying
        ordering = ['store_url']


class ShopifyProduct(models.Model):
    # Relationship to ShopifyStore through store_url
    home_url = models.ForeignKey(ShopifyStore, to_field='store_url', db_column='home_url',
                                 related_name='shopify_products', related_query_name='product', on_delete=models.CASCADE)
    # Primary key field for the product's ID
    product_id = models.CharField(max_length=100, primary_key=True)
    # Title of the product
    product_title = models.CharField(max_length=200)
    # Price of the product
    price = models.FloatField(default=0.0)
    # Timestamp when the product was created
    created_at = models.DateTimeField(default=timezone.now)
    # Timestamp when the product was last updated
    updated_at = models.DateTimeField(default=timezone.now)
    # Total sales for different time periods
    total_sales_1 = models.IntegerField(default=0)
    total_sales_3 = models.IntegerField(default=0)
    total_sales_7 = models.IntegerField(default=0)
    total_sales_14 = models.IntegerField(default=0)
    total_sales_30 = models.IntegerField(default=0)
    total_sales_90 = models.IntegerField(default=0)
    # Index indicating best selling product
    best_selling_index = models.IntegerField(default=0)
    # Index indicating trending product
    trending_index = models.IntegerField(default=0)
    # Total sales of the product
    total_sales = models.IntegerField(default=0)
    # Total sales in the last day
    total_sales_last_day = models.IntegerField(default=0)
    # URL for the product's thumbnail
    thumbnail_url = models.URLField(max_length=1000)
    # URL for the product
    product_url = models.URLField(max_length=1000)
    # Flag indicating whether the product is saved
    saved = models.BooleanField(default=False)
    # Timestamp when the product was last saved
    last_saved = models.DateTimeField(default=timezone.now)
    # Description of the product
    description = models.CharField(max_length=10000, blank=True)
    # JSON field for daily sales chart
    daily_sales_chart = models.CharField(
        max_length=1000, default='{"1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0}')

    class Meta:
        # Define the default ordering for querying
        ordering = ['total_sales']


class ProductPurchase(models.Model):
    # Unique purchase ID
    purchase_id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
        editable=False,
        unique=True)
    # Relationship to ShopifyProduct through product_id
    product_id = models.ForeignKey(ShopifyProduct, to_field='product_id', db_column='product_id',
                                   related_name='product_purchases', related_query_name='purchase', on_delete=models.CASCADE)
    # Timestamp when data was crawled
    crawled_at = models.DateTimeField(
        default=timezone.now)
    # Timestamp when the product was purchased
    purchasing_time = models.DateTimeField(default=timezone.now)
    # Timestamp when the record was created
    created_at = models.DateTimeField(default=timezone.now)
    # Store URL related to the purchase
    store_url = models.URLField(max_length=255, default='', blank=True)
