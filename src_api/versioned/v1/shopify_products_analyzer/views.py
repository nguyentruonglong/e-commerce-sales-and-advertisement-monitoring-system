import json
import logging
import urllib
from datetime import datetime, timedelta
from io import BytesIO

import dateutil.parser
# import matplotlib
import pytz
import functools
import validators
from django.contrib.auth.decorators import login_required
from django.db.models import (Case, Count, ExpressionWrapper, F, IntegerField,
                              Max, Q, Subquery, Value, When)
from django.http import HttpResponse, JsonResponse, QueryDict, request
from django.utils import timezone
from django.views import View
from matplotlib import pyplot as plt
from rest_framework import status, viewsets
from rest_framework.pagination import DjangoPaginator, PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView

from versioned.v1.shopify_products_analyzer import charts

from .models import ProductPurchase, ShopifyProduct, ShopifyStore
from .serializers import (PartialStoreSerializer, ShopifyProductSerializer,
                          ShopifyStoreSerializer, DailySalesSerializer)

# matplotlib.use('Agg')

logger = logging.getLogger(__name__)


class ShopifyStoreViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.serializer_class = ShopifyStoreSerializer
        self.queryset = ShopifyStore.objects.all()

    # @method_decorator(cache_page(60*1))
    def list(self, request):
        try:
            serializer = PartialStoreSerializer(
                instance=self.queryset, many=True)
            data_response = Response(serializer.data)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class ShopifyProductViewSet(viewsets.ModelViewSet):
    def __init__(self):
        # Initialize instance variables
        self.timestamp = timezone.localtime(timezone.now())
        self.serializer_class = ShopifyProductSerializer

        # Define the base queryset for ShopifyProducts with annotations to include related fields and filter by store_status
        self.queryset = ShopifyProduct.objects.annotate(
            website_title=F('home_url__website_title'),
            last_crawling=F('home_url__last_crawling'),
            currency_code=F('home_url__currency_code'),
            store_status=F('home_url__store_status'),
        ).filter(store_status=True)

        # Initialize query parameters with default values
        self.query_params = {
            'tab': 'all-products',
            'period': 'all-time',
            'purchasing_time': 'all-time',
            'sort_by': 'published-time',
            'product_title': str(),
            'store_url': str(),
        }

    def get_queryset(self):
        return self.queryset

    # @method_decorator(cache_page(60*1))
    def list(self, request, *args, **kwargs):
        try:
            # Set pagination class
            self.pagination_class = PageNumberPagination

            # Define a dictionary to map 'period' values to filter parameters
            period_filters = {
                '1-day': timedelta(days=1),
                '3-days': timedelta(days=3),
                '7-days': timedelta(days=7),
                '14-days': timedelta(days=14),
                '30-days': timedelta(days=30),
                '90-days': timedelta(days=90),
            }

            # Apply period filter based on 'period' value
            period_value = self.query_params['period']
            if period_value in period_filters:
                period = period_filters[period_value]
                self.queryset = self.queryset.filter(
                    created_at__gte=self.timestamp - period)

            # Handle 'store_url' and 'product_title' filters
            if self.query_params['store_url'] != str():
                query_search = urllib.parse.unquote_plus(
                    self.query_params['store_url'])
                if validators.re_path(query_search):
                    self.queryset = self.queryset.filter(home_url=query_search)
                else:
                    self.queryset = self.queryset.filter(
                        Q(product_title__icontains=query_search))

            if self.query_params['product_title'] != str():
                product_title = urllib.parse.unquote_plus(
                    self.query_params['product_title'])
                self.queryset = self.queryset.filter(
                    product_title=product_title)

            # Define a dictionary to map 'tab' and 'sort_by' values to orderings
            tab_orderings = {
                'all-products': {
                    'published-time': ['-created_at', 'home_url', 'best_selling_index', 'product_title'],
                    'last-sale': ['-updated_at', 'home_url', 'best_selling_index', 'product_title'],
                    'trending': ['-trending_index', 'home_url', 'best_selling_index', 'product_title'],
                },
                'saved': {'default': ['-last_saved', 'home_url', 'best_selling_index', 'product_title']},
                'best-selling': {
                    'total-sales': {
                        '1-day': ['-total_sales_1', 'home_url', 'best_selling_index', 'product_title'],
                        '3-days': ['-total_sales_3', 'home_url', 'best_selling_index', 'product_title'],
                        '7-days': ['-total_sales_7', 'home_url', 'best_selling_index', 'product_title'],
                        '14-days': ['-total_sales_14', 'home_url', 'best_selling_index', 'product_title'],
                        '30-days': ['-total_sales_30', 'home_url', 'best_selling_index', 'product_title'],
                        '90-days': ['-total_sales_90', 'home_url', 'best_selling_index', 'product_title'],
                        'default': ['-total_sales', 'home_url', 'best_selling_index', 'product_title'],
                    },
                },
            }

            # Apply tab and sort_by ordering
            tab_value = self.query_params['tab']
            sort_by_value = self.query_params['sort_by']
            if tab_value in tab_orderings:
                if sort_by_value in tab_orderings[tab_value]:
                    ordering = tab_orderings[tab_value][sort_by_value]
                else:
                    # If sort_by is not recognized, use the default ordering
                    ordering = tab_orderings[tab_value].get('default', None)
                if ordering:
                    self.queryset = self.queryset.order_by(*ordering)

            # Paginate the queryset and serialize the data
            page = self.paginate_queryset(self.queryset)
            serializer_context = {'request': request}
            serializer = self.serializer_class(
                page, context=serializer_context, many=True
            )

            data_response = self.get_paginated_response(serializer.data)
        except Exception as e:
            # Handle exceptions and return an error response
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    # Apply caching for 60 seconds (1 minute)
    # @method_decorator(cache_page(60*1))
    # Use database transaction
    # @transaction.atomic
    def create(self, request, *args, **kwargs):
        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)

            # Check if 'store_url' and 'store_status' are present in the request data
            if not all(key in request.data for key in ['store_url', 'store_status']):
                data_response = JsonResponse(
                    {'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                updated_flag = 0
                requested_store_url = request.data['store_url']
                requested_store_status = request.data['store_status']
                requested_category = request.data.get('category', 'shopify')
                print(requested_store_url)

                # If the store is no longer active
                if requested_store_status is False:
                    # Update the store status to 'False'
                    ShopifyStore.objects.filter(
                        store_url=requested_store_url).update(store_status=requested_store_status)
                # If the store is active and the request data is complete
                elif all(key in request.data for key in ['redirected_url', 'currency_code', 'website_title', 'last_crawling', 'products_info']):
                    # If the original store URL was redirected
                    if request.data['store_url'] != request.data['redirected_url']:
                        # Get the original URL before redirection
                        old_domain = request.data['store_url']
                        # Get the current URL of the store
                        requested_store_url = request.data['redirected_url']

                        # Delete data related to the original URL
                        ShopifyStore.objects.filter(
                            store_url=request.data['store_url']).delete()
                        # Create a new object with the redirected URL
                        ShopifyStore.objects.update_or_create(
                            store_url=requested_store_url,
                            defaults={
                                'old_domain': old_domain,
                                'store_status': requested_store_status,
                                'category': requested_category}
                        )

                    currency_code = request.data['currency_code']
                    website_title = request.data['website_title']
                    last_crawling = dateutil.parser.parse(
                        request.data['last_crawling']).astimezone(pytz.timezone('Etc/UTC'))
                    requested_products_info = request.data['products_info']

                    # Extract and sort product IDs
                    input_product_ids = list(requested_products_info.keys())
                    input_product_ids.sort()

                    # Filter current product objects and get their IDs
                    filtered_product_objs = ShopifyProduct.objects.filter(
                        home_url=ShopifyStore.objects.get(store_url=requested_store_url)).values('product_id')
                    current_product_ids = list(
                        map(lambda e: e['product_id'], filtered_product_objs))

                    # Check if input product IDs match current product IDs
                    if functools.reduce(lambda x, y: x and y, map(lambda p, q: p == q, input_product_ids, current_product_ids), True):
                        updated_flag = 1
                    else:
                        updated_flag = 0

                    # Update store information
                    ShopifyStore.objects.filter(store_url=requested_store_url).update(website_title=website_title,
                                                                                      last_crawling=last_crawling,
                                                                                      currency_code=currency_code,
                                                                                      store_status=requested_store_status,
                                                                                      updated_number=F(
                                                                                          'updated_number') + updated_flag,
                                                                                      category=requested_category)

                    # Find deleted product IDs
                    deleted_product_ids = [
                        product_id for product_id in current_product_ids if product_id not in input_product_ids]

                    # Delete product objects with deleted IDs
                    ShopifyProduct.objects.filter(home_url=ShopifyStore.objects.get(
                        store_url=requested_store_url), product_id__in=deleted_product_ids).delete()

                    # Extract created and updated product objects
                    created_product_objects = [requested_products_info[product_id]
                                               for product_id in input_product_ids if product_id not in current_product_ids]

                    updated_product_objects = [requested_products_info[product_id]
                                               for product_id in input_product_ids if product_id in current_product_ids]

                    # Delete product objects with deleted IDs
                    ShopifyProduct.objects.filter(
                        product_id__in=deleted_product_ids).delete()

                    # Create new product objects for created products
                    ShopifyProduct.objects.bulk_create([ShopifyProduct(
                        product_id=product_obj['product_id'],
                        product_title=product_obj['product_title'],
                        price=float(product_obj['price']),
                        created_at=dateutil.parser.parse(
                            product_obj['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                        updated_at=dateutil.parser.parse(
                            product_obj['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                        best_selling_index=int(
                            product_obj['best_selling_index']),
                        thumbnail_url=product_obj['thumbnail_url'],
                        product_url=product_obj['product_url'],
                        home_url=ShopifyStore.objects.get(
                            store_url=requested_store_url),
                    ) for product_obj in created_product_objects])

                    # Get the updated product objects
                    updated_product_queryset = [ShopifyProduct.objects.get(product_id=product_obj['product_id'])
                                                for product_obj in updated_product_objects]

                    # Update the properties of updated product objects
                    for i_th in range(len(updated_product_objects)):
                        updated_product_queryset[i_th].product_title = updated_product_objects[i_th]['product_title']
                        updated_product_queryset[i_th].price = float(
                            updated_product_objects[i_th]['price'])
                        updated_product_queryset[i_th].created_at = dateutil.parser.parse(
                            updated_product_objects[i_th]['created_at']).astimezone(pytz.timezone('Etc/UTC'))
                        updated_product_queryset[i_th].updated_at = dateutil.parser.parse(
                            updated_product_objects[i_th]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))
                        updated_product_queryset[i_th].best_selling_index = int(
                            updated_product_objects[i_th]['best_selling_index'])
                        updated_product_queryset[i_th].thumbnail_url = updated_product_objects[i_th]['thumbnail_url']
                        updated_product_queryset[i_th].product_url = updated_product_objects[i_th]['product_url']
                        updated_product_queryset[i_th].home_url = ShopifyStore.objects.get(
                            store_url=requested_store_url)

                    # Bulk update the updated product objects
                    ShopifyProduct.objects.bulk_update(
                        updated_product_queryset,
                        ['product_title', 'price', 'created_at', 'updated_at', 'best_selling_index', 'thumbnail_url', 'product_url', 'home_url'])

                    # Calculate start times for various sales periods
                    total_sales_1_starttime = self.timestamp - \
                        timedelta(hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    total_sales_3_starttime = self.timestamp - \
                        timedelta(days=2, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    total_sales_7_starttime = self.timestamp - \
                        timedelta(days=6, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    total_sales_14_starttime = self.timestamp - \
                        timedelta(days=13, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    total_sales_30_starttime = self.timestamp - \
                        timedelta(days=29, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    total_sales_90_starttime = self.timestamp - \
                        timedelta(days=89, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    trending_index_starttime = self.timestamp - \
                        timedelta(days=2, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    last_day_starttime = self.timestamp - \
                        timedelta(hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    # Get existing product purchase data
                    existing_product_purchases = list(ProductPurchase.objects.filter(product_id__in=input_product_ids).values(
                        'product_id').annotate(latest_purchase=Max('purchasing_time')))

                    # Check if there are missing product purchases
                    if len(existing_product_purchases) != len(input_product_ids):
                        existing_product_ids = (
                            map(lambda e: e['product_id'], existing_product_purchases))

                        new_product_ids = list(
                            set(input_product_ids)-set(existing_product_ids))

                        # Create new product purchase objects for missing products
                        ProductPurchase.objects.bulk_create([ProductPurchase(
                            crawled_at=timezone.localtime(
                                timezone.now()),
                            product_id=ShopifyProduct.objects.get(
                                product_id=product_id),
                            purchasing_time=dateutil.parser.parse(
                                requested_products_info[product_id]['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                            created_at=dateutil.parser.parse(
                                requested_products_info[product_id]['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                            store_url=requested_store_url,
                        ) for product_id in new_product_ids])

                        for product_id in new_product_ids:
                            existing_product_purchases.append({'product_id': product_id, 'latest_purchase': dateutil.parser.parse(
                                requested_products_info[product_id]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))})

                    # Filter requested product objects with updated purchase dates
                    requested_product_objects = [requested_products_info[product_purchase_object['product_id']]
                                                 for product_purchase_object in existing_product_purchases if product_purchase_object['latest_purchase'] < dateutil.parser.parse(
                        requested_products_info[product_purchase_object['product_id']]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))]

                    # Create product purchase objects for requested products with updated purchase dates
                    ProductPurchase.objects.bulk_create([ProductPurchase(
                        crawled_at=timezone.localtime(
                            timezone.now()),
                        product_id=ShopifyProduct.objects.get(
                            product_id=product_object['product_id']),
                        purchasing_time=dateutil.parser.parse(
                            product_object['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                        created_at=dateutil.parser.parse(
                            product_object['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                        store_url=requested_store_url,
                    ) for product_object in requested_product_objects if product_object['updated_at'] != product_object['created_at']])

                    # Calculate daily sales chart data
                    cartesian_coordinates_objects = charts.calculate_daily_sales_chart(
                        input_product_ids, self.timestamp)

                    # Get total objects for product updates
                    total_objects = [ShopifyProduct.objects.get(product_id=input_product_id)
                                     for input_product_id in input_product_ids]

                    # Query product purchase data
                    product_purchase_queryset = ProductPurchase.objects.filter(product_id__in=input_product_ids).values(
                        'product_id').annotate(
                        total_sales_1=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_1_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_3=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_3_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_7=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_7_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_14=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_14_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_30=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_30_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_90=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_90_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales=Count(
                            'purchasing_time', distinct=True, filter=Q(purchasing_time__gt=F('created_at'))),
                        trending_index=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            trending_index_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_last_day=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            last_day_starttime), purchasing_time__gt=F('created_at'))),
                    )

                    # Sort the product purchase queryset
                    product_purchase_queryset = sorted(
                        product_purchase_queryset, key=lambda obj: obj['product_id'])

                    # Update total objects with sales data
                    for i_th in range(len(input_product_ids)):
                        total_objects[i_th].total_sales_1 = product_purchase_queryset[i_th]['total_sales_1']
                        total_objects[i_th].total_sales_3 = product_purchase_queryset[i_th]['total_sales_3']
                        total_objects[i_th].total_sales_7 = product_purchase_queryset[i_th]['total_sales_7']
                        total_objects[i_th].total_sales_14 = product_purchase_queryset[i_th]['total_sales_14']
                        total_objects[i_th].total_sales_30 = product_purchase_queryset[i_th]['total_sales_30']
                        total_objects[i_th].total_sales_90 = product_purchase_queryset[i_th]['total_sales_90']

                        total_objects[i_th].total_sales = product_purchase_queryset[i_th]['total_sales']
                        total_objects[i_th].trending_index = product_purchase_queryset[i_th]['trending_index']
                        total_objects[i_th].total_sales_last_day = product_purchase_queryset[i_th]['total_sales_last_day']
                        total_objects[i_th].daily_sales_chart = json.dumps(
                            cartesian_coordinates_objects[i_th])

                    # Bulk update the total objects with sales data
                    ShopifyProduct.objects.bulk_update(
                        total_objects,
                        ['total_sales_1', 'total_sales_3', 'total_sales_7', 'total_sales_14', 'total_sales_30', 'total_sales_90', 'total_sales', 'trending_index', 'total_sales_last_day', 'daily_sales_chart'])

        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    def partial_update(self, request, *args, **kwargs):
        try:
            # Check if 'product_id' and 'last_saved' are present in the request data
            if not all(key in request.data for key in ['product_id', 'last_saved']):
                return JsonResponse({'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)

            product_id = request.data['product_id']
            last_saved_str = request.data['last_saved']
            last_saved_time = dateutil.parser.parse(last_saved_str)

            # Update the ShopifyProduct object
            ShopifyProduct.objects.filter(product_id=product_id).update(
                saved=Case(When(saved=True, then=False), default=True),
                last_saved=last_saved_time
            )

            data_response = JsonResponse({}, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class DailySalesChartViewSet(viewsets.ModelViewSet):
    # This class handles a view for a daily sales chart
    serializer_class = DailySalesSerializer

    def get_queryset(self):
        return self.queryset

    # @method_decorator(cache_page(60*1))
    def list(self, request):
        try:
            # Check if 'product_id' is in the query parameters
            if all(key in request.query_params for key in ['product_id']):
                # Define chart axis and initialize variables
                x_min = 1
                x_max = 7
                y_min = 0
                product_sales = list()

                # Get the current timestamp
                timestamp = timezone.localtime(timezone.now())

                # Get the requested product ID from the query parameters
                requested_product_id = int(request.query_params['product_id'])

                # Create a buffer for storing the chart
                buffered = BytesIO()
                # Clear any previous plots
                plt.clf()
                plt.figure(figsize=(5, 5), frameon=True)
                plt.box(on=False)
                plt.xticks(ticks=[], labels=None)
                dates = range(x_min, x_max + 1)

                # Retrieve daily sales data for the requested product
                product_sales = [
                    ShopifyProduct.objects.filter(product_id=requested_product_id).annotate(
                        total_sales_per_day=Count('purchase', filter=Q(purchase__purchasing_time__range=(
                            # Calculate the date range for each day in the chart
                            timestamp - timedelta(days=x_max - date, hours=timestamp.hour,
                                                  minutes=timestamp.minute, seconds=timestamp.second),
                            timestamp
                            if timestamp < timestamp - timedelta(days=x_max - date - 1, hours=timestamp.hour,
                                                                 minutes=timestamp.minute, seconds=timestamp.second)
                            else timestamp - timedelta(days=x_max - date - 1, hours=timestamp.hour,
                                                       minutes=timestamp.minute, seconds=timestamp.second)
                        )))
                    ).values()[0]['total_sales_per_day']
                    for date in dates
                ]

                # Calculate the maximum value for the y-axis
                y_max = max(product_sales)

                # Set the chart axis limits and style
                plt.axis([x_min - 0.5, x_max + 0.5, y_min - 0.5, y_max + 0.5])
                plt.yticks(ticks=range(y_max + 1), labels=None)

                # Add horizontal lines for the chart background
                for y in range(y_max + 1):
                    plt.plot([0, x_max + 0.5], [y, y], '',
                             color='#575050', label="", linewidth=0.8)

                # Plot the daily sales data
                plt.plot(dates, product_sales, 'o-', color='#ffcd42',
                         label="", linewidth=6, markersize=16)

                plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
                plt.tight_layout()

                # Save the chart as a PNG image
                plt.savefig(buffered, format="png", transparent=False)

                # Create an HTTP response with the chart image
                data_response = HttpResponse(
                    buffered.getvalue(), content_type='image/png')
            else:
                # If 'product_id' is not provided in the query parameters, return an error response
                data_response = JsonResponse(
                    {'error': 'Invalid input format'}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            # Handle exceptions and return an error response
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            # Clean up Matplotlib resources
            if hasattr(plt, 'clf') and callable(plt.clf):
                plt.clf()
            if hasattr(plt, 'close') and callable(plt.close):
                plt.close()

            # Return the HTTP response with the chart or an error message
            return data_response
