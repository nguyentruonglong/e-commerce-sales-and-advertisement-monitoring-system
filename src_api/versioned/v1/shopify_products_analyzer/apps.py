from django.apps import AppConfig


class ShopifyProductsAnalyzerConfig(AppConfig):
    name = 'versioned.v1.shopify_products_analyzer'
    label = 'shopify_products_analyzer_v1'