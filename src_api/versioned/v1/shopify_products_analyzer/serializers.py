from . models import ShopifyStore, ShopifyProduct
from rest_framework import serializers


# Serializer for the ShopifyProduct model
class ShopifyProductSerializer(serializers.ModelSerializer):
    # Serialize the website title as a character field
    website_title = serializers.CharField()
    # Serialize the last crawling timestamp as a date-time field
    last_crawling = serializers.DateTimeField()
    # Serialize the currency code as a character field
    
    class Meta:
        # Use the ShopifyProduct model
        model = ShopifyProduct
        # Define the default ordering for querying
        ordering = ['total_sales']
        # Include all fields in the serialization
        fields = '__all__'
        # Set the reference name
        ref_name = 'v1'


# Serializer for the ShopifyStore model
class ShopifyStoreSerializer(serializers.ModelSerializer):
    class Meta:
        # Use the ShopifyStore model
        model = ShopifyStore
        # Define the default ordering for querying
        ordering = ['store_url']
        # Include all fields in the serialization
        fields = '__all__'
        # Set the reference name
        ref_name = 'v1'


# Partial serializer for the ShopifyStore model
class PartialStoreSerializer(serializers.ModelSerializer):
    class Meta:
        # Use the ShopifyStore model
        model = ShopifyStore
        # Include specific fields in the serialization
        fields = ('store_url', 'store_status', 'category')
        # Set the reference name
        ref_name = 'v1'


class DailySalesSerializer(serializers.Serializer):

    class Meta:
        # Set the reference name
        ref_name = 'v1'
