from datetime import datetime, timedelta
from django.utils import timezone
from django.db.models import Count, F, Q
from versioned.v1.shopify_products_analyzer.models import ProductPurchase

# Function to calculate the start time of the day
def calculate_start_of_day(x_max, date, timestamp):
    return timestamp - timedelta(days=x_max - date, hours=timestamp.hour, minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond)

# Function to calculate the end time of the day
def calculate_end_of_day(x_max, date, timestamp):
    end_of_day = timestamp if timestamp < calculate_start_of_day(x_max, date + 1, timestamp) else calculate_start_of_day(x_max, date, timestamp)
    return end_of_day

# Function to calculate daily sales chart
def calculate_daily_sales_chart(input_product_ids, timestamp):
    try:
        x_min, x_max = 1, 7
        dates = list(range(x_min, x_max + 1))

        sorted_cartesian_coordinates = []

        for date in dates:
            day_filter = Q(
                purchasing_time__range=(
                    calculate_start_of_day(x_max, date, timestamp),
                    calculate_end_of_day(x_max, date, timestamp)
                ),
                purchasing_time__gt=F('created_at')
            )

            cartesian_coordinates = ProductPurchase.objects.filter(
                product_id__in=input_product_ids
            ).values('product_id').annotate(
                **{f'day_{date}': Count('purchasing_time', distinct=True, filter=day_filter)}
            )

            sorted_cartesian_coordinates.extend([
                {str(date): obj[f'day_{date}']} for obj in cartesian_coordinates
            ])

    except Exception as e:
        print(e)
        sorted_cartesian_coordinates = []
        for product_id in input_product_ids:
            tmp_cartesian_coordinates_object = {str(date): 0 for date in dates}
            sorted_cartesian_coordinates.append(tmp_cartesian_coordinates_object)

    return sorted_cartesian_coordinates
