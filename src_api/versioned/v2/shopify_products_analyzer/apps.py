from django.apps import AppConfig


class ShopifyProductsAnalyzerConfig(AppConfig):
    name = 'versioned.v2.shopify_products_analyzer'
    label = 'shopify_products_analyzer_v2'