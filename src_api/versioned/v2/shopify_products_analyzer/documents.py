from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from .models import ShopifyProduct, ShopifyStore


@registry.register_document
class ShopifyStoreDocument(Document):

    class Index:
        name = 'shopify_store'
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 1}

    class Django:
        model = ShopifyStore

        fields = [
            'store_url',
            'shopify_store_url',
            'shopify_store_id',
            'website_title',
            'scraped_at',
            'currency_code',
            'store_status',
            'is_shopify_store',
            'category',
            'created_at',
            'updated_at',
        ]

        ignore_signals = False


@registry.register_document
class ShopifyProductDocument(Document):
    shopify_store_id = fields.TextField()

    total_sales_last_1_day = fields.IntegerField()
    total_sales_last_2_days = fields.IntegerField()
    total_sales_last_3_days = fields.IntegerField()
    total_sales_last_4_days = fields.IntegerField()
    total_sales_last_5_days = fields.IntegerField()
    total_sales_last_6_days = fields.IntegerField()
    total_sales_last_7_days = fields.IntegerField()
    total_sales_last_8_days = fields.IntegerField()
    total_sales_last_9_days = fields.IntegerField()
    total_sales_last_10_days = fields.IntegerField()
    total_sales_last_11_days = fields.IntegerField()
    total_sales_last_12_days = fields.IntegerField()
    total_sales_last_13_days = fields.IntegerField()
    total_sales_last_14_days = fields.IntegerField()
    total_sales_last_21_days = fields.IntegerField()
    total_sales_last_28_days = fields.IntegerField()
    total_sales_last_30_days = fields.IntegerField()
    total_sales_last_60_days = fields.IntegerField()
    total_sales_last_90_days = fields.IntegerField()
    total_sales = fields.IntegerField()

    daily_sales_chart = fields.ObjectField(
        properties={
            'last_1_day': fields.IntegerField(),
            'last_2_days': fields.IntegerField(),
            'last_3_days': fields.IntegerField(),
            'last_4_days': fields.IntegerField(),
            'last_5_days': fields.IntegerField(),
            'last_6_days': fields.IntegerField(),
            'last_7_days': fields.IntegerField(),
            'last_8_day': fields.IntegerField(),
            'last_9_days': fields.IntegerField(),
            'last_10_days': fields.IntegerField(),
            'last_11_days': fields.IntegerField(),
            'last_12_days': fields.IntegerField(),
            'last_13_days': fields.IntegerField(),
            'last_14_days': fields.IntegerField()
    })

    class Index:
        name = 'shopify_product'
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 1}

    class Django:
        model = ShopifyProduct

        fields = [
            'product_id',
            'product_title',
            'price',
            'thumbnail_url',
            'product_url',
            'description',
            'created_at',
            'updated_at',
        ]

        ignore_signals = False

        # auto_refresh = False
        # queryset_pagination = 100

    def prepare_daily_sales_chart(self, instance):
        return instance.daily_sales_chart
    
    def prepare_total_sales_last_1_day(self, instance):
        return instance.total_sales_last_1_day
    
    def prepare_total_sales_last_2_days(self, instance):
        return instance.total_sales_last_2_days

    def prepare_total_sales_last_3_days(self, instance):
        return instance.total_sales_last_3_days

    def prepare_total_sales_last_4_days(self, instance):
        return instance.total_sales_last_4_days

    def prepare_total_sales_last_5_days(self, instance):
        return instance.total_sales_last_5_days

    def prepare_total_sales_last_6_days(self, instance):
        return instance.total_sales_last_6_days
    
    def prepare_total_sales_last_7_days(self, instance):
        return instance.total_sales_last_7_days
    
    def prepare_total_sales_last_8_days(self, instance):
        return instance.total_sales_last_8_days

    def prepare_total_sales_last_9_days(self, instance):
        return instance.total_sales_last_9_days
    
    def prepare_total_sales_last_10_days(self, instance):
        return instance.total_sales_last_10_days
    
    def prepare_total_sales_last_11_days(self, instance):
        return instance.total_sales_last_11_days
    
    def prepare_total_sales_last_12_days(self, instance):
        return instance.total_sales_last_12_days

    def prepare_total_sales_last_13_days(self, instance):
        return instance.total_sales_last_13_days

    def prepare_total_sales_last_14_days(self, instance):
        return instance.total_sales_last_14_days

    def prepare_total_sales_last_21_days(self, instance):
        return instance.total_sales_last_21_days

    def prepare_total_sales_last_28_days(self, instance):
        return instance.total_sales_last_28_days

    def prepare_total_sales_last_30_days(self, instance):
        return instance.total_sales_last_30_days

    def prepare_total_sales_last_60_days(self, instance):
        return instance.total_sales_last_60_days

    def prepare_total_sales_last_90_days(self, instance):
        return instance.total_sales_last_90_days

    def prepare_total_sales(self, instance):
        return instance.total_sales