import base64
import calendar
import json
import logging
import operator
import os
import queue
import random
import re
import threading
import time
import urllib
from datetime import datetime, timedelta
from io import BytesIO
from multiprocessing import Lock, Process, Queue
from multiprocessing.pool import ThreadPool
from urllib import parse

import dateutil.parser
import pytz
import functools
import validators
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core import paginator
from django.core.files.storage import FileSystemStorage
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import transaction
from django.db.models import (Case, Count, ExpressionWrapper, F, IntegerField,
                              Max, Q, Subquery, Value, When)
from django.db.models.functions import Coalesce
from django.http import HttpResponse, JsonResponse, QueryDict, request
from django.shortcuts import render
from django.template import RequestContext
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django_filters.rest_framework import DjangoFilterBackend
from furl import furl
from rest_framework import filters, generics, permissions, status, viewsets
from rest_framework.authentication import (BasicAuthentication,
                                           SessionAuthentication,
                                           TokenAuthentication)
from rest_framework.decorators import (action, api_view,
                                       authentication_classes,
                                       permission_classes)
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView
from rest_framework.pagination import DjangoPaginator, PageNumberPagination

from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_RANGE,
    LOOKUP_QUERY_EXCLUDE,
    LOOKUP_QUERY_ISNULL,
    LOOKUP_QUERY_IN,
    LOOKUP_QUERY_CONTAINS,
)
from django_elasticsearch_dsl_drf.filter_backends import (
    DefaultOrderingFilterBackend,
    OrderingFilterBackend,
    FilteringFilterBackend,
)
from django_elasticsearch_dsl_drf.viewsets import (
    BaseDocumentViewSet,
)

from versioned.v2.shopify_products_analyzer import charts

from versioned.v2.shopify_products_analyzer.models import ProductPurchase, ShopifyProduct, ShopifyStore
from versioned.v2.shopify_products_analyzer.serializers import (PartialStoreSerializer, ShopifyProductSerializer,
                          ShopifyStoreSerializer)

from .documents import ShopifyProductDocument, ShopifyStoreDocument

from .serializers import ShopifyProductDocumentSerializer, ShopifyStoreDocumentSerializer


logger = logging.getLogger(__name__)



class ShopifyStoreViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.serializer_class = ShopifyStoreSerializer
        self.queryset = ShopifyStore.objects.all()

    # @method_decorator(cache_page(60*1))
    def list(self, request):
        try:
            serializer = PartialStoreSerializer(
                instance=self.queryset, many=True)
            data_response = Response(serializer.data)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class ShopifyProductViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.timestamp = timezone.localtime(timezone.now())
        self.serializer_class = ShopifyProductSerializer

        # self.queryset = ShopifyProduct.objects.annotate(
            # website_title=F('home_url__website_title'),
            # scraped_at=F('home_url__scraped_at'),
            # currency_code=F('home_url__currency_code'),
            # store_status=F('home_url__store_status'),
        # ).filter(store_status=True)

        self.queryset = ShopifyProduct.objects.all()
        
        self.query_params = {
            'tab': 'all-products',
            'period': 'all-time',
            'purchasing_time': 'all-time',
            'sort_by': 'published-time',
            'product_title': str(),
            'store_url': str(),
        }

    def get_queryset(self):
        return self.queryset

    # @method_decorator(cache_page(60*1))
    def list(self, request, *args, **kwargs):
        try:
            self.pagination_class = PageNumberPagination
            for key in request.query_params:
                if key in self.query_params:
                    self.query_params[key] = request.query_params[key]

            if (self.query_params['period'] in ['1-day', '3-days', '7-days', '14-days', '30-days', '90-days']):
                period = int(self.query_params['period'].replace(
                    'days', str()).replace('day', str()).replace('-', str()))
                self.queryset = self.queryset.filter(created_at__gte=self.timestamp-timedelta(
                    days=period, hours=self.timestamp.hour, minutes=self.timestamp.minute, seconds=self.timestamp.second))

            if self.query_params['store_url'] != str():
                query_search = urllib.parse.unquote_plus(
                    (self.query_params['store_url']))
                if validators.re_path(query_search):
                    self.queryset = self.queryset.filter(home_url=query_search)
                else:
                    self.queryset = self.queryset.filter(
                        Q(product_title__icontains=query_search))

            if self.query_params['product_title'] != str():
                product_title = urllib.parse.unquote_plus(
                    str(self.query_params['product_title']))
                self.queryset = self.queryset.filter(
                    product_title=product_title)

            if (self.query_params['tab'] == 'all-products'):
                if (self.query_params['sort_by'] == 'published-time'):
                    self.queryset = self.queryset.order_by(
                        '-created_at', 'home_url', 'best_selling_index', 'product_title')
                elif (self.query_params['sort_by'] == 'last-sale'):
                    self.queryset = self.queryset.order_by(
                        '-updated_at', 'home_url', 'best_selling_index', 'product_title')
                elif (self.query_params['sort_by'] == 'trending'):
                    self.queryset = self.queryset.order_by(
                        '-trending_index', 'home_url', 'best_selling_index', 'product_title')

            elif (self.query_params['tab'] == 'saved'):
                self.queryset = self.queryset.filter(saved=True).order_by(
                    '-last_saved', 'home_url', 'best_selling_index', 'product_title')
            elif (self.query_params['tab'] == 'best-selling'):
                self.queryset = self.queryset.filter(~Q(home_url__in=['https://trendingcustom.com', 'https://famvibe.com']))
                if (self.query_params['sort_by'] == 'total-sales'):
                    if (self.query_params['purchasing_time'] in ['1-day', '3-days', '7-days', '14-days', '30-days', '90-days']):
                        purchasing_time = self.query_params['purchasing_time'].replace(
                            'days', str()).replace('day', str()).replace('-', str())
                        self.queryset = self.queryset.order_by(
                            f'-total_sales_{purchasing_time}', 'home_url', 'best_selling_index', 'product_title')
                    else:
                        self.queryset = self.queryset.order_by(
                            '-total_sales', 'home_url', 'best_selling_index', 'product_title')

            page = self.paginate_queryset(self.queryset)
            serializer_context = {'request': request}
            serializer = self.serializer_class(
                page, context=serializer_context, many=True
            )

            data_response = self.get_paginated_response(serializer.data)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    # @method_decorator(cache_page(60*1))
    # @transaction.atomic
    def create(self, request, *args, **kwargs):

        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)
            if not all(key in request.data for key in ['store_url', 'store_status']):
                data_response = JsonResponse(
                    {'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                updated_flag = 0
                requested_store_url = request.data['store_url']
                requested_store_status = request.data['store_status']
                requested_category = request.data.get(
                    'category', str('shopify'))
                print(requested_store_url)
                # Nếu như store không còn hoạt động
                if requested_store_status == False:
                    # Cập nhật trạng thái mới là store đã không còn hoạt động
                    ShopifyStore.objects.filter(
                        store_url=requested_store_url).update(store_status=requested_store_status)
                # Nếu như store vẫn còn hoạt động thì kiểm tra dữ liệu có đầy đủ hay không
                elif all(key in request.data for key in ['redirected_url', 'currency_code', 'website_title', 'scraped_at', 'products_info']):
                    # Nếu như store vẫn còn hoạt động nhưng tên miền gốc bị chuyển hướng sang tên miền mới
                    if request.data['store_url'] != request.data['redirected_url']:
                        # Lấy URL tên miền cũ trước khi bị chuyển hưởng của store
                        old_domain = request.data['store_url']
                        # Lấy URL hiện tại của store
                        requested_store_url = request.data['redirected_url']

                        # Xóa dữ liệu liên quan đến tên miền gốc
                        ShopifyStore.objects.filter(
                            store_url=request.data['store_url']).delete()
                        # Tạo đối tượng mới với URL sau khi URL gốc bị chuyển hướng
                        ShopifyStore.objects.update_or_create(
                            store_url=requested_store_url,
                            defaults={
                                'old_domain': old_domain,
                                'store_status': requested_store_status,
                                'category': requested_category}
                        )

                    currency_code = request.data['currency_code']
                    website_title = request.data['website_title']
                    scraped_at = dateutil.parser.parse(
                        request.data['scraped_at']).astimezone(pytz.timezone('Etc/UTC'))
                    requested_products_info = request.data['products_info']

                    input_product_ids = list(requested_products_info.keys())
                    input_product_ids.sort()

                    filtered_product_objs = ShopifyProduct.objects.filter(
                        home_url=ShopifyStore.objects.get(store_url=requested_store_url)).values('product_id')
                    current_product_ids = list(
                        map(lambda e: e['product_id'], filtered_product_objs))

                    if functools.reduce(lambda x, y: x and y, map(lambda p, q: p == q, input_product_ids, current_product_ids), True):
                        updated_flag = 1
                    else:
                        updated_flag = 0

                    ShopifyStore.objects.filter(store_url=requested_store_url).update(website_title=website_title,
                                                                                      scraped_at=scraped_at,
                                                                                      currency_code=currency_code,
                                                                                      store_status=requested_store_status,
                                                                                      updated_number=F(
                                                                                          'updated_number') + updated_flag,
                                                                                      category=requested_category,
                                                                                      )

                    deleted_product_ids = [
                        product_id for product_id in current_product_ids if product_id not in input_product_ids]
                    ShopifyProduct.objects.filter(home_url=ShopifyStore.objects.get(
                        store_url=requested_store_url), product_id__in=deleted_product_ids).delete()

                    created_product_objects = [requested_products_info[product_id]
                                               for product_id in input_product_ids if product_id not in current_product_ids]

                    updated_product_objects = [requested_products_info[product_id]
                                               for product_id in input_product_ids if product_id in current_product_ids]

                    ShopifyProduct.objects.filter(
                        product_id__in=deleted_product_ids).delete()

                    ShopifyProduct.objects.bulk_create([ShopifyProduct(
                        product_id=product_obj['product_id'],
                        product_title=product_obj['product_title'],
                        price=float(product_obj['price']),
                        created_at=dateutil.parser.parse(
                            product_obj['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                        updated_at=dateutil.parser.parse(
                            product_obj['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                        best_selling_index=int(
                            product_obj['best_selling_index']),
                        thumbnail_url=product_obj['thumbnail_url'],
                        product_url=product_obj['product_url'],
                        home_url=ShopifyStore.objects.get(
                            store_url=requested_store_url),
                    ) for product_obj in created_product_objects])

                    updated_product_queryset = [ShopifyProduct.objects.get(product_id=product_obj['product_id'])
                                                for product_obj in updated_product_objects]

                    for i_th in range(len(updated_product_objects)):
                        updated_product_queryset[i_th].product_title = updated_product_objects[i_th]['product_title']
                        updated_product_queryset[i_th].price = float(
                            updated_product_objects[i_th]['price'])
                        updated_product_queryset[i_th].created_at = dateutil.parser.parse(
                            updated_product_objects[i_th]['created_at']).astimezone(pytz.timezone('Etc/UTC'))
                        updated_product_queryset[i_th].updated_at = dateutil.parser.parse(
                            updated_product_objects[i_th]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))
                        updated_product_queryset[i_th].best_selling_index = int(
                            updated_product_objects[i_th]['best_selling_index'])
                        updated_product_queryset[i_th].thumbnail_url = updated_product_objects[i_th]['thumbnail_url']
                        updated_product_queryset[i_th].product_url = updated_product_objects[i_th]['product_url']
                        updated_product_queryset[i_th].home_url = ShopifyStore.objects.get(
                            store_url=requested_store_url)

                    ShopifyProduct.objects.bulk_update(
                        updated_product_queryset,
                        ['product_title', 'price', 'created_at', 'updated_at', 'best_selling_index', 'thumbnail_url', 'product_url', 'home_url'])

                    total_sales_1_starttime = self.timestamp - \
                        timedelta(hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    total_sales_3_starttime = self.timestamp - \
                        timedelta(days=2, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    total_sales_7_starttime = self.timestamp - \
                        timedelta(days=6, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    total_sales_14_starttime = self.timestamp - \
                        timedelta(days=13, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    total_sales_30_starttime = self.timestamp - \
                        timedelta(days=29, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    total_sales_90_starttime = self.timestamp - \
                        timedelta(days=89, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    trending_index_starttime = self.timestamp - \
                        timedelta(days=2, hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)
                    last_day_starttime = self.timestamp - \
                        timedelta(hours=self.timestamp.hour, minutes=self.timestamp.minute,
                                  seconds=self.timestamp.second, microseconds=self.timestamp.microsecond)

                    existing_product_purchases = list(ProductPurchase.objects.filter(product_id__in=input_product_ids).values(
                        'product_id').annotate(latest_purchase=Max('purchasing_time')))

                    if len(existing_product_purchases) != len(input_product_ids):
                        existing_product_ids = (
                            map(lambda e: e['product_id'], existing_product_purchases))

                        new_product_ids = list(
                            set(input_product_ids)-set(existing_product_ids))

                        ProductPurchase.objects.bulk_create([ProductPurchase(
                            crawled_at=timezone.localtime(
                                timezone.now()),
                            product_id=ShopifyProduct.objects.get(
                                product_id=product_id),
                            purchasing_time=dateutil.parser.parse(
                                requested_products_info[product_id]['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                            created_at=dateutil.parser.parse(
                                requested_products_info[product_id]['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                            store_url=requested_store_url,
                        ) for product_id in new_product_ids])

                        for product_id in new_product_ids:
                            existing_product_purchases.append({'product_id': product_id, 'latest_purchase': dateutil.parser.parse(
                                requested_products_info[product_id]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))})

                    requested_product_objects = [requested_products_info[product_purchase_object['product_id']]
                                                 for product_purchase_object in existing_product_purchases if product_purchase_object['latest_purchase'] < dateutil.parser.parse(
                        requested_products_info[product_purchase_object['product_id']]['updated_at']).astimezone(pytz.timezone('Etc/UTC'))]

                    ProductPurchase.objects.bulk_create([ProductPurchase(
                        crawled_at=timezone.localtime(
                            timezone.now()),
                        product_id=ShopifyProduct.objects.get(
                            product_id=product_object['product_id']),
                        purchasing_time=dateutil.parser.parse(
                            product_object['updated_at']).astimezone(pytz.timezone('Etc/UTC')),
                        created_at=dateutil.parser.parse(
                            product_object['created_at']).astimezone(pytz.timezone('Etc/UTC')),
                        store_url=requested_store_url,
                    ) for product_object in requested_product_objects if product_object['updated_at'] != product_object['created_at']])

                    cartesian_coordinates_objects = charts.calculate_daily_sales_chart(
                        input_product_ids, self.timestamp)

                    total_objects = [ShopifyProduct.objects.get(product_id=input_product_id)
                                     for input_product_id in input_product_ids]

                    product_purchase_queryset = ProductPurchase.objects.filter(product_id__in=input_product_ids).values(
                        'product_id').annotate(
                        total_sales_1=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_1_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_3=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_3_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_7=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_7_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_14=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_14_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_30=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_30_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_90=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            total_sales_90_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales=Count(
                            'purchasing_time', distinct=True, filter=Q(purchasing_time__gt=F('created_at'))),
                        trending_index=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            trending_index_starttime), purchasing_time__gt=F('created_at'))),
                        total_sales_last_day=Count('purchasing_time', distinct=True, filter=Q(purchasing_time__gte=(
                            last_day_starttime), purchasing_time__gt=F('created_at'))),

                    )

                    product_purchase_queryset = sorted(
                        product_purchase_queryset, key=lambda obj: obj['product_id'])

                    for i_th in range(len(input_product_ids)):
                        total_objects[i_th].total_sales_1 = product_purchase_queryset[i_th]['total_sales_1']
                        total_objects[i_th].total_sales_3 = product_purchase_queryset[i_th]['total_sales_3']
                        total_objects[i_th].total_sales_7 = product_purchase_queryset[i_th]['total_sales_7']
                        total_objects[i_th].total_sales_14 = product_purchase_queryset[i_th]['total_sales_14']
                        total_objects[i_th].total_sales_30 = product_purchase_queryset[i_th]['total_sales_30']
                        total_objects[i_th].total_sales_90 = product_purchase_queryset[i_th]['total_sales_90']

                        total_objects[i_th].total_sales = product_purchase_queryset[i_th]['total_sales']
                        total_objects[i_th].trending_index = product_purchase_queryset[i_th]['trending_index']
                        total_objects[i_th].total_sales_last_day = product_purchase_queryset[i_th]['total_sales_last_day']
                        total_objects[i_th].daily_sales_chart = json.dumps(
                            cartesian_coordinates_objects[i_th])

                    ShopifyProduct.objects.bulk_update(
                        total_objects,
                        ['total_sales_1', 'total_sales_3', 'total_sales_7', 'total_sales_14', 'total_sales_30', 'total_sales_90', 'total_sales', 'trending_index', 'total_sales_last_day', 'daily_sales_chart'])

                # Nếu dữ liệu được gửi đến không đầy đủ thông tin
                else:
                    data_response = JsonResponse(
                        {'error': 'Invalid data format'}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': e.args[0]}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class ShopifyStoreDocumentView(BaseDocumentViewSet):
    
    document = ShopifyStoreDocument
    serializer_class = ShopifyStoreDocumentSerializer
    filter_backends = [FilteringFilterBackend,]
    filter_fields = {
        'shopify_store_id': {
            'field': 'shopify_store_id',
            'lookups': [
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_CONTAINS,
            ],
        },

    }

    ordering_fields = {
    }

    ordering = 'shopify_store_id'

class ShopifyProductDocumentView(BaseDocumentViewSet):

    document = ShopifyProductDocument
    serializer_class = ShopifyProductDocumentSerializer
    filter_backends = [FilteringFilterBackend,]
    filter_fields = {
        'shopify_store_id': {
            'field': 'shopify_store_id',
            'lookups': [
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_CONTAINS,
            ],
        },

    }

    ordering_fields = {
    }
    
    ordering = 'shopify_store_id'


class DataUploadViewSet(viewsets.ViewSet):

    @staticmethod
    def _read_csv_file_into_list(csv_file_object):
        inserted_urls = list()
        csv_file_content = csv_file_object.read()
        csv_file_content = csv_file_content.decode('utf-8')
        input_urls = csv_file_content.replace('\r', str())
        input_urls = input_urls.strip().split('\n')
        input_urls = list(set(input_urls))

        url_regex = '(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)'

        for input_url in input_urls:
            if not validators.re_path(input_url):
                matched_urls = re.findall(url_regex, input_url)
                if bool(matched_urls):
                    input_url = matched_urls[0].strip()
                else:
                    continue

            url_structure = furl(input_url)
            input_url = url_structure.origin
            inserted_urls.append(input_url)

        inserted_urls = list(set(inserted_urls))

        return inserted_urls


    def create(self, request, *args, **kwargs):
        try:
            data_response = JsonResponse({}, status=status.HTTP_200_OK)

            if 'shopify_stores_file' not in request.FILES:
                return data_response

            csv_file_object = request.FILES['shopify_stores_file']
            extension = os.path.splitext(csv_file_object.name)[1]
            
            if not (extension == '.txt' or extension == '.csv'):
                return data_response

            filtered_store_urls = self._read_csv_file_into_list(csv_file_object)

            for store_url in filtered_store_urls:
                ShopifyStore.objects.create(store_url=store_url)
            return data_response
        except Exception as e:
            print(e)
            data_response = JsonResponse(
                {'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
            return data_response


