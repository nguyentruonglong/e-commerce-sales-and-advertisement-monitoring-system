from django.urls import path
from django.urls import include, re_path
from . import views as shopify_product_views
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import RedirectView

urlpatterns = [
    # path('shopify-product-search', views.ShopifyProductDocumentView.as_view({'get': 'list'})),
    # path('shopify-store-search', views.ShopifyStoreDocumentView.as_view({'get': 'list'})),
    re_path(r'shopify-products', shopify_product_views.ShopifyProductViewSet.as_view(
            {'get': 'list', 'post': 'create', 'patch': 'partial_update'})),
]
