from datetime import datetime, timedelta
from django.utils import timezone
from django.db.models import Count, F, Q
from versioned.v2.shopify_products_analyzer.models import ProductPurchase


def get_starttime_of_day(x_max, date, timestamp):
    return timestamp - timedelta(days=x_max - date, hours=timestamp.hour,
                                 minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond)


def get_endtime_of_day(x_max, date, timestamp):
    return timestamp if timestamp < timestamp - timedelta(days=x_max - date - 1, hours=timestamp.hour, minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond) else timestamp - timedelta(days=x_max - date - 1, hours=timestamp.hour, minutes=timestamp.minute, seconds=timestamp.second, microseconds=timestamp.microsecond)


def calculate_daily_sales_chart(input_product_ids, timestamp):
    try:
        x_min = 1
        x_max = 7
        cartesian_coordinates_objects = dict()
        sorted_cartesian_coordinates_objects = list()
        dates = list(range(x_min, x_max+1))

        cartesian_coordinates_objects = ProductPurchase.objects.filter(
            product_id__in=input_product_ids).values('product_id').annotate(
                day_1=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 1, timestamp),
                            get_endtime_of_day(x_max, 1, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                ),
                day_2=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 2, timestamp),
                            get_endtime_of_day(x_max, 2, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                ),
                day_3=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 3, timestamp),
                            get_endtime_of_day(x_max, 3, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                ),
                day_4=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 4, timestamp),
                            get_endtime_of_day(x_max, 4, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                ),
                day_5=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 5, timestamp),
                            get_endtime_of_day(x_max, 5, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                ),
                day_6=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 6, timestamp),
                            get_endtime_of_day(x_max, 6, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                ),
                day_7=Count(
                    'purchasing_time', distinct=True, filter=Q(
                        purchasing_time__range=(
                            get_starttime_of_day(x_max, 7, timestamp),
                            get_endtime_of_day(x_max, 7, timestamp)
                        ),
                        purchasing_time__gt=F('created_at')
                    )
                )
        )

        cartesian_coordinates_objects = sorted(
            cartesian_coordinates_objects, key=lambda obj: obj['product_id'])

        for cartesian_coordinates_object in cartesian_coordinates_objects:
            tmp_cartesian_coordinates_object = dict()
            for date in dates:
                tmp_cartesian_coordinates_object[str(
                    date)] = cartesian_coordinates_object['day_' + str(date)]
            sorted_cartesian_coordinates_objects.append(
                tmp_cartesian_coordinates_object)
    except Exception as e:
        print(e)
        sorted_cartesian_coordinates_objects = list()
        for product_id in input_product_ids:
            tmp_cartesian_coordinates_object = dict()
            for date in dates:
                tmp_cartesian_coordinates_object.update({str(date): 0})
            sorted_cartesian_coordinates_objects.append(
                tmp_cartesian_coordinates_object)
    finally:
        return sorted_cartesian_coordinates_objects
