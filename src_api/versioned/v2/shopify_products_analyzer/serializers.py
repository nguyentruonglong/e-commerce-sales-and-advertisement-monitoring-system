from rest_framework import serializers
from django_elasticsearch_dsl_drf.serializers import DocumentSerializer
from . models import ShopifyStore, ShopifyProduct
from .documents import ShopifyProductDocument, ShopifyStoreDocument

class ShopifyProductSerializer(serializers.ModelSerializer):
    website_title = serializers.CharField()
    scraped_at = serializers.DateTimeField()
    currency_code = serializers.CharField()

    class Meta:
        model = ShopifyProduct
        ordering = ['total_sales']
        fields = '__all__'
        ref_name = 'v2'

class ShopifyStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopifyStore
        ordering = ['store_url']
        fields = '__all__'
        ref_name = 'v2'

class PartialStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopifyStore
        fields = ('store_url', 'store_status', 'category')
        ref_name = 'v2'


class ShopifyStoreDocumentSerializer(DocumentSerializer):
    """Serializer for ShopifyStore document."""

    class Meta(object):
        document = ShopifyStoreDocument
        fields = (
            'store_url',
            'shopify_store_url',
            'shopify_store_id',
            'website_title',
            'scraped_at',
            'currency_code',
            'store_status',
            'is_shopify_store',
            'category',
            'created_at',
            'updated_at',
        )

        ref_name = 'v2'
        

class ShopifyProductDocumentSerializer(DocumentSerializer):
    """Serializer for ShopifyProduct document."""

    class Meta(object):
        document = ShopifyProductDocument
        fields = (
            'product_id',
            'product_title',
            'shopify_store_id',
            'price',
            'thumbnail_url',
            'product_url',
            'description',
            'created_at',
            'updated_at',
            'total_sales_last_1_day',
            'total_sales_last_2_days',
            'total_sales_last_3_days',
            'total_sales_last_4_days',
            'total_sales_last_5_days',
            'total_sales_last_6_days',
            'total_sales_last_7_days',
            'total_sales_last_8_days',
            'total_sales_last_9_days',
            'total_sales_last_10_days',
            'total_sales_last_11_days',
            'total_sales_last_12_days',
            'total_sales_last_13_days',
            'total_sales_last_14_days',
            'total_sales_last_21_days',
            'total_sales_last_28_days',
            'total_sales_last_30_days',
            'total_sales_last_60_days',
            'total_sales_last_90_days',
            'total_sales',
            'daily_sales_chart'
        )

        ref_name = 'v2'