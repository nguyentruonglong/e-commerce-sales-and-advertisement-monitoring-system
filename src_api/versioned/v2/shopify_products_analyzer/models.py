import uuid
from django.db import models
from django.utils import timezone
from django.db.models import F
from django.contrib.postgres.fields import JSONField
from django.utils.crypto import get_random_string

from react_django_template.models import BaseModel

class ShopifyStore(BaseModel):
    uuid = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
        editable=False,
        unique=True,
        db_column='uuid')
    shopify_store_id = models.CharField(
        max_length=256, db_column='shopify_store_id', unique=True, default=str(), blank=True)
    store_url = models.CharField(
        max_length=256, default=str(), blank=True)
    shopify_store_url = models.CharField(max_length=256, default=str(), blank=True)
    website_title = models.CharField(
        max_length=1024, default=str(), blank=True)
    scraped_at = models.DateTimeField(auto_now=True)
    currency_code = models.CharField(max_length=8, default='USD', blank=True)
    store_status = models.BooleanField(default=True)
    category = models.CharField(max_length=256, default=str(), blank=True)
    is_shopify_store = models.BooleanField(default=False)

    class Meta:
        ordering = [F('store_url').desc(nulls_last=True)]
        indexes = [
            models.Index(fields=['store_url']),
            models.Index(fields=['shopify_store_id']),
            models.Index(fields=['shopify_store_url']),
            models.Index(fields=['is_shopify_store']),
        ]

        unique_together = ['store_url', 'shopify_store_url']

    def save(self,*args, **kwargs):
        if not bool(self.shopify_store_id):
            self.shopify_store_id = get_random_string(32).lower()
        if not bool(self.shopify_store_url):
            self.shopify_store_url = get_random_string(32).lower()

        super(ShopifyStore, self).save(*args, **kwargs)


class ShopifyProduct(BaseModel):
    product_id = models.CharField(
        max_length=128, unique=True, primary_key=True, db_column='product_id')
    shopify_store_id = models.ForeignKey(ShopifyStore, to_field='shopify_store_id', db_column='shopify_store_id',
                                 related_name='shopify_products', related_query_name='shopify_product', on_delete=models.CASCADE, default=str())
    product_title = models.CharField(max_length=256, default=str(), blank=True)
    price = models.FloatField(default=0.0)
    thumbnail_url = models.CharField(max_length=1024, default=str(), blank=True)
    product_url = models.CharField(max_length=1024, default=str(), blank=True)
    description = models.CharField(max_length=4096, default=str(), blank=True)

    class Meta:
        ordering = [F('shopify_store_id').desc(nulls_last=True)]
        indexes = [
            models.Index(fields=['shopify_store_id', 'product_id']),
            models.Index(fields=['shopify_store_id']),
            models.Index(fields=['product_id']),
            models.Index(fields=['product_title'])
        ]


class ProductPurchase(BaseModel):
    purchase_id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
        editable=False,
        unique=True,
        db_column='purchase_id')
    product_id = models.ForeignKey(ShopifyProduct, to_field='product_id', db_column='product_id',
                                   related_name='product_purchases', related_query_name='product_purchase', on_delete=models.CASCADE)
    shopify_store_id = models.ForeignKey(ShopifyStore, to_field='shopify_store_id', db_column='shopify_store_id',
                                  related_name='product_purchases', related_query_name='product_purchase', on_delete=models.CASCADE, default=str())

    scraped_at = models.DateTimeField(
        default=timezone.now)
    purchasing_time = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = [F('shopify_store_id').desc(nulls_last=True)]
        indexes = [
            models.Index(fields=['shopify_store_id', 'product_id']),
            models.Index(fields=['shopify_store_id']),
            models.Index(fields=['product_id']),
        ]
