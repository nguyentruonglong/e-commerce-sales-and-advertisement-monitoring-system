from django.urls import path
from django.urls import include, re_path
from django.views.decorators.csrf import csrf_exempt
from . import views as frontend_template_view


# Define the URL patterns for your Django application
urlpatterns = [
    # Map the root URL to the ReactDjangoTemplateView class view with CSRF exemption
    path('', csrf_exempt(
        frontend_template_view.ReactDjangoTemplateView.as_view()), name='root'),
    # Map the 'facebook-ads' URL to the ReactDjangoTemplateView class view with CSRF exemption
    path('facebook-ads', csrf_exempt(frontend_template_view.ReactDjangoTemplateView.as_view()),
         name='facebook_ads'),
    # Map the 'saved-video-ads' URL to the ReactDjangoTemplateView class view with CSRF exemption
    path('saved-video-ads', csrf_exempt(frontend_template_view.ReactDjangoTemplateView.as_view()),
         name='saved_video_ads'),
    # Map the 'video-posts-analytics' URL to the ReactDjangoTemplateView class view with CSRF exemption
    path('video-posts-analytics', csrf_exempt(frontend_template_view.ReactDjangoTemplateView.as_view()),
         name='video_posts_analytics'),
    # Map the 'facebook-video-posts' URL to the ReactDjangoTemplateView class view with CSRF exemption
    path('facebook-video-posts', csrf_exempt(frontend_template_view.ReactDjangoTemplateView.as_view()),
         name='facebook_video_posts'),
    # Map the 'data-upload' URL to the DataUploadView class view with CSRF exemption
    path('data-upload', csrf_exempt(frontend_template_view.ReactDjangoTemplateView.as_view()), name='data_upload'),

    # Use the UserAuthView class to handle login page requests with 'login' path
    path('user/login', csrf_exempt(frontend_template_view.UserAuthView.as_view()),
         name='user_login'),

    # Use the UserAuthView class to handle logout button requests with 'logout' path
    path('user/logout', csrf_exempt(frontend_template_view.UserAuthView.as_view()),
         name='user_logout'),

    # Use the AccountManagerView class to handle requests for resetting passwords with 'reset-password' path
    path('user/reset-password', csrf_exempt(
        frontend_template_view.AccountManagerView.as_view()), name='reset_password'),

    # View to display a list of Shopify Stores and associated Facebook Pages when clicking the "Shopify Stores" tab.
    path('data-manager', csrf_exempt(frontend_template_view.ShopifyStoreManagerView.as_view()),
         name='shopify_stores_tab'),

    # View to display a list of ShopBase Stores and associated Facebook Pages when clicking the "Shopbase Stores" tab.
    path('data-manager/shopbase-stores', csrf_exempt(
        frontend_template_view.ShopbaseStoreManagerView.as_view()), name='shopbase_stores_tab'),

    # Extract and pass the URL of a ShopBase Store to search, matching the regex from a GET request to the corresponding view method.
    re_path(r'^data-manager/shopbase-stores/search-store/(?P<store_url_query>(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+))/$',
            frontend_template_view.ShopbaseStoreManagerView.as_view(), name='search_store'),

    # Extract and pass the URL of a ShopBase Store to delete, matching the regex from a DELETE request to the corresponding view method.
    re_path(r'^data-manager/shopbase-stores/delete-store/(?P<shopify_store_url>(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+))/$',
            frontend_template_view.ShopbaseStoreManagerView.as_view(), name='delete_store'),

    # View to download data files.
    path('data-manager/download',
         csrf_exempt(frontend_template_view.DataReporter.as_view()), name='download_data'),

    # Extract and pass the URL of a Shopify Store to search, matching the regex from a GET request to the corresponding view method.
    re_path(r'^data-manager/search-store/(?P<store_url_query>(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+))/$',
            frontend_template_view.ShopifyStoreManagerView.as_view(), name='search_store'),

    # Extract and pass the URL of a Shopify Store to delete, matching the regex from a DELETE request to the corresponding view method.
    re_path(r'^data-manager/delete-store/(?P<shopify_store_url>(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+))/$',
            frontend_template_view.ShopifyStoreManagerView.as_view(), name='delete_store'),

    # View to display a list of Facebook Pages and the associated Store when clicking the "Facebook Pages" tab.
    path('data-manager/facebook-pages', csrf_exempt(
        frontend_template_view.FacebookPageManagerView.as_view()), name='facebook_pages_tab'),

    # Extract and pass the Facebook Page URL to search, matching the regex from a GET request to the corresponding view method.
    re_path(r'^data-manager/facebook-pages/search-page/(?P<page_url_query>(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+))/$',
            frontend_template_view.FacebookPageManagerView.as_view(), name='search_page'),

    # Extract and pass the Facebook Page ID to delete, matching the regex from a DELETE request to the corresponding view method.
    re_path(r'^data-manager/facebook-pages/delete-page/(?P<facebook_page_id>([0-9]{10,25}))/$',
            frontend_template_view.FacebookPageManagerView.as_view(), name='delete_page'),

    # View to display a list of Video Posts and the associated Facebook Page when clicking the "Facebook Video Posts" tab.
    path('data-manager/facebook-video-posts', csrf_exempt(
        frontend_template_view.FacebookVideoPostManagerView.as_view()), name='facebook_video_posts_tab'),

    # Extract and pass the Video Post URL to search, matching the regex from a GET request to the corresponding view method.
    re_path(r'^data-manager/facebook-video-posts/search-post/(?P<post_url_query>(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+))/$',
            frontend_template_view.FacebookVideoPostManagerView.as_view(), name='search_post'),

    # Extract and pass the Facebook Post ID of the Video Post to delete, matching the regex from a DELETE request to the corresponding view method.
    re_path(r'^data-manager/facebook-video-posts/delete-post/(?P<facebook_post_id>([0-9]{10,25}))/$',
            frontend_template_view.FacebookVideoPostManagerView.as_view(), name='delete_post'),
]
