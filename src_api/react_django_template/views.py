
import csv
import urllib.parse
import validators

from furl import furl
from rest_framework import status
from django.db import transaction
from django.db.models import OuterRef, Q, Subquery
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login, logout


from django.shortcuts import render, redirect
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.http import url_has_allowed_host_and_scheme

from versioned.v1.facebook_ads_analyzer.models import FacebookAd, FacebookPage
from versioned.v1.facebook_posts_analyzer.models import FacebookVideoPost
from versioned.v1.shopify_products_analyzer.models import ShopifyStore


class ReactDjangoTemplateView(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    def get(self, request):
        return render(request, 'base.html')


class UserAuthView(TemplateView):
    # Return the login page interface when receiving a GET request to /user/login
    def get(self, request):
        return render(request, 'login.html')

    # Handle login authentication when receiving a POST request to /user/login
    def post(self, request):
        # Log the user out to ensure a clean login session
        logout(request)

        # Get the URL to redirect to after successful login
        redirect_to = request.POST.get(
            'redirect_to', request.GET.get('redirect_to', '/'))

        # Ensure the redirect URL is a safe URL
        redirect_to = (redirect_to if url_has_allowed_host_and_scheme(
            redirect_to, request.get_host()) else '/')

        try:
            # Get the username and password from the form on the login page
            username = request.POST.get('username')
            password = request.POST.get('password')

            if username is not None and password is not None:
                # Authenticate the user based on the provided username and password
                user = authenticate(
                    request, username=username, password=password)

                if user is not None:
                    # If authentication is successful, log in the user
                    login(request, user)
                    return HttpResponseRedirect(redirect_to or '/')
                else:
                    # If authentication fails, redirect back to the login page
                    raise Exception("Authentication failed")

            else:
                # If username or password is missing, redirect back to the login page
                raise Exception("Username or password is missing")

        except Exception as e:
            # Handle exceptions and log them
            print(e)
            return redirect(f'/user/login?redirect_to={redirect_to}')


class AccountManagerView(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    # Return the reset password page interface when receiving a GET request to /user/reset-password
    def get(self, request):
        return render(request, 'reset.html')


class ShopifyStoreManagerView(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        '''
        GET request handler for the ShopifyStoreManagerView.
        This method processes GET requests to display and paginate Shopify Stores on the "Shopify Stores" tab.
        '''

        # Get the current page number from the request's query parameters
        page_number = request.GET.get('page')
        # Get the search query from the request's query parameters
        search_query = request.GET.get('search_query')
        # Get the store status from the request's query parameters
        store_status = request.GET.get('status')
        # Get the action query from the request's query parameters
        action_query = request.GET.get('action_query')

        # Query all ShopifyStore objects excluding those with 'shopbase' in their category
        shopify_store_queryset = ShopifyStore.objects.filter(
            ~Q(category__contains='shopbase'))

        if action_query is not None:
            if action_query == 'delete-all':
                # Extract checked objects from the request's query parameters and delete them
                checked_objects = list(
                    map(lambda x: urllib.parse.unquote_plus(x), list(request.GET.getlist('checked_objects'))))
                ShopifyStore.objects.filter(
                    store_url__in=checked_objects).delete()

        if search_query is not None:
            # Decode and filter ShopifyStore objects by search query
            search_query = urllib.parse.unquote_plus(search_query)
            shopify_store_queryset = shopify_store_queryset.filter(
                store_url__contains=search_query)

        if store_status is not None:
            if store_status == 'working':
                # Filter ShopifyStore objects by 'working' status
                shopify_store_queryset = shopify_store_queryset.filter(
                    store_status=True)
            elif store_status == 'not-working':
                # Filter ShopifyStore objects by 'not-working' status
                shopify_store_queryset = shopify_store_queryset.filter(
                    store_status=False)
            elif store_status == 'non-updated':
                # Filter ShopifyStore objects by 'non-updated' status
                shopify_store_queryset = shopify_store_queryset.filter(
                    updated_number__gt=30 * 48)

        # Annotate Facebook Page information with product URLs associated with Facebook Ads
        page_object_queryset = FacebookPage.objects.annotate(
            product_url=Subquery(
                FacebookAd.objects.filter(
                    page_id=OuterRef('page_id')
                ).values('product_url')[:1]
            ),
        ).values('page_url', 'page_name', 'product_url')

        # Create a paginator for ShopifyStore objects with a page size of 50
        shopify_paginator = Paginator(shopify_store_queryset, 50)
        # Get the current page object
        shopify_page_object = shopify_paginator.get_page(page_number)

        # Render the 'shopify_stores.html' template with processed data
        return render(request, 'shopify_stores.html', {'shopify_page_object': shopify_page_object, 'page_object_queryset': page_object_queryset})

    def delete(self, request, *args, **kwargs):
        '''
        DELETE request handler for the ShopifyStoreManagerView.
        This method processes DELETE requests to remove a Shopify Store with a specified primary key taken from the URL using AJAX.
        '''

        try:
            data_response = HttpResponse(status=status.HTTP_200_OK)
            if 'shopify_store_url' in self.kwargs:
                deleted_store_url = self.kwargs['shopify_store_url']
                # Delete the ShopifyStore object with the specified store URL
                ShopifyStore.objects.filter(
                    store_url=deleted_store_url).delete()
            else:
                data_response = HttpResponse(
                    status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class ShopbaseStoreManagerView(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        '''
        GET request handler for the ShopbaseStoreManagerView.
        This method processes GET requests to display and paginate ShopBase Stores on the "ShopBase Stores" tab.
        '''

        # Get the current page number from the request's query parameters
        page_number = request.GET.get('page')
        # Get the search query from the request's query parameters
        search_query = request.GET.get('search_query')
        # Get the store status from the request's query parameters
        store_status = request.GET.get('status')
        # Get the action query from the request's query parameters
        action_query = request.GET.get('action_query')

        # Query all ShopBaseStore objects
        shopbase_store_queryset = ShopifyStore.objects.filter(
            category='shopbase')

        if action_query is not None:
            if action_query == 'delete-all':
                # Extract checked objects from the request's query parameters and delete them
                checked_objects = list(
                    map(lambda x: urllib.parse.unquote_plus(x), list(request.GET.getlist('checked_objects'))))
                ShopifyStore.objects.filter(
                    store_url__in=checked_objects).delete()

        if search_query is not None:
            # Decode and filter ShopBaseStore objects by search query
            search_query = urllib.parse.unquote_plus(search_query)
            shopbase_store_queryset = shopbase_store_queryset.filter(
                store_url__contains=search_query)

        if store_status is not None:
            if store_status == 'working':
                # Filter ShopBaseStore objects by 'working' status
                shopbase_store_queryset = shopbase_store_queryset.filter(
                    store_status=True)
            elif store_status == 'not-working':
                # Filter ShopBaseStore objects by 'not-working' status
                shopbase_store_queryset = shopbase_store_queryset.filter(
                    store_status=False)

        # Annotate Facebook Page information with product URLs associated with Facebook Ads
        page_object_queryset = FacebookPage.objects.annotate(
            product_url=Subquery(
                FacebookAd.objects.filter(
                    page_id=OuterRef('page_id')
                ).values('product_url')[:1]
            ),
        ).values('page_url', 'page_name', 'product_url')

        # Create a paginator for ShopBaseStore objects with a page size of 50
        shopbase_paginator = Paginator(shopbase_store_queryset, 50)
        # Get the current page object
        shopbase_page_object = shopbase_paginator.get_page(page_number)

        # Render the 'shopbase_stores.html' template with processed data
        return render(request, 'shopbase_stores.html', {'shopbase_page_object': shopbase_page_object, 'page_object_queryset': page_object_queryset})

    def delete(self, request, *args, **kwargs):
        '''
        DELETE request handler for the ShopbaseStoreManagerView.
        This method processes DELETE requests to delete ShopBase Stores using the primary key of the object extracted from the URL in an AJAX request.
        '''

        try:
            data_response = HttpResponse(status=status.HTTP_200_OK)
            if ('shopify_store_url' in self.kwargs):
                deleted_store_url = self.kwargs['shopify_store_url']
                # Delete the ShopBaseStore object with the specified store URL
                ShopifyStore.objects.filter(
                    store_url=deleted_store_url).delete()
            else:
                data_response = HttpResponse(
                    status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class FacebookPageManagerView(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    def get(self, request):
        '''
        GET request handler for the FacebookPageManagerView.
        This method processes GET requests to display and paginate Facebook Pages on the "Facebook Pages" tab.
        '''

        # Get the current page number from the request's query parameters
        page_number = request.GET.get('page')
        # Get the search query from the request's query parameters
        search_query = request.GET.get('search_query')
        # Get the page status from the request's query parameters
        page_status = request.GET.get('status')
        # Get the action query from the request's query parameters
        action_query = request.GET.get('action_query')

        # Query all FacebookPage objects and annotate them with product URL information
        page_object_queryset = FacebookPage.objects.annotate(
            product_url=Subquery(
                FacebookAd.objects.filter(
                    page_id=OuterRef('page_id')
                ).values('product_url')[:1]
            ),
        ).values('page_id', 'page_url', 'page_name', 'product_url', 'page_status')

        if action_query is not None:
            if action_query == 'delete-all':
                # Extract checked objects from the request's query parameters and delete them
                checked_objects = list(
                    map(lambda x: urllib.parse.unquote_plus(x), list(request.GET.getlist('checked_objects'))))
                FacebookPage.objects.filter(
                    page_id__in=checked_objects).delete()

        if search_query is not None:
            # Decode and filter FacebookPage objects by search query
            search_query = urllib.parse.unquote_plus(search_query)
            page_object_queryset = page_object_queryset.filter(
                page_url__contains=search_query)

        if page_status is not None:
            if page_status == 'working':
                # Filter FacebookPage objects by 'working' status
                page_object_queryset = page_object_queryset.filter(
                    page_status=True)
            elif page_status == 'not-working':
                # Filter FacebookPage objects by 'not-working' status
                page_object_queryset = page_object_queryset.filter(
                    page_status=False)

        # Process and clean product URLs
        for page_object in page_object_queryset:
            if page_object['product_url'] is not None:
                if validators.re_path(page_object['product_url']):
                    page_object['product_url'] = furl(
                        page_object['product_url'].strip()).origin
                else:
                    page_object['product_url'] = str()
            else:
                page_object['product_url'] = str()

        # Create a paginator for FacebookPage objects with a page size of 50
        facebook_page_paginator = Paginator(page_object_queryset, 50)
        # Get the current page object
        facebook_page_object = facebook_page_paginator.get_page(page_number)

        # Render the 'facebook_pages.html' template with processed data
        return render(request, 'facebook_pages.html', {'facebook_page_object': facebook_page_object})

    def delete(self, request, *args, **kwargs):
        '''
        DELETE request handler for the FacebookPageManagerView.
        This method processes DELETE requests to delete Facebook Pages using the primary key of the object extracted from the URL in an AJAX request.
        '''
        try:
            data_response = HttpResponse(status=status.HTTP_200_OK)
            if 'facebook_page_id' not in self.kwargs:
                data_response = HttpResponse(
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                deleted_page_id = self.kwargs['facebook_page_id']
                FacebookPage.objects.filter(page_id=deleted_page_id).delete()
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class FacebookVideoPostManagerView(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    def get(self, request):
        '''
        GET request handler for the FacebookVideoPostManagerView.
        This method processes GET requests to display and paginate Facebook Video Posts on the "Facebook Video Posts" tab.
        '''

        # Get the current page number from the request's query parameters
        page_number = request.GET.get('page')
        # Get the search query from the request's query parameters
        search_query = request.GET.get('search_query')
        # Get the post status from the request's query parameters
        post_status = request.GET.get('status')
        # Get the action query from the request's query parameters
        action_query = request.GET.get('action_query')

        # Query all FacebookVideoPost objects
        facebook_post_queryset = FacebookVideoPost.objects.all()

        if action_query is not None:
            if action_query == 'delete-all':
                # Extract checked objects from the request's query parameters and delete them
                checked_objects = list(
                    map(lambda x: urllib.parse.unquote_plus(x), list(request.GET.getlist('checked_objects'))))
                # Delete all selected objects
                FacebookVideoPost.objects.filter(
                    post_id__in=checked_objects).delete()

        if search_query is not None:
            # Decode and filter FacebookVideoPost objects by search query
            search_query = urllib.parse.unquote_plus(search_query)
            facebook_post_queryset = facebook_post_queryset.filter(
                post_url__contains=search_query)

        if post_status is not None:
            if post_status == 'working':
                # Filter FacebookVideoPost objects by 'working' status
                facebook_post_queryset = facebook_post_queryset.filter(
                    post_status=True)
            elif post_status == 'not-working':
                # Filter FacebookVideoPost objects by 'not-working' status
                facebook_post_queryset = facebook_post_queryset.filter(
                    post_status=False)

        # Process and clean Facebook Page URL for each Facebook Video Post
        for post_object in facebook_post_queryset:
            if post_object.facebook_page_url is not None:
                if validators.re_path(post_object.facebook_page_url):
                    post_object.facebook_page_url = post_object.facebook_page_url.strip()
                else:
                    post_object.facebook_page_url = str()
            else:
                post_object.facebook_page_url = str()

        # Create a paginator for FacebookVideoPost objects with a page size of 50
        facebook_post_paginator = Paginator(facebook_post_queryset, 50)
        # Get the current page object
        facebook_post_object = facebook_post_paginator.get_page(page_number)

        # Render the 'facebook_video_posts.html' template with processed data
        return render(request, 'facebook_video_posts.html', {'facebook_post_object': facebook_post_object})

    def delete(self, request, *args, **kwargs):
        '''
        DELETE request handler for the FacebookVideoPostManagerView.
        This method processes DELETE requests to delete Facebook Video Posts using the primary key of the object extracted from the URL in an AJAX request.
        '''

        try:
            data_response = HttpResponse(status=status.HTTP_200_OK)
            if 'facebook_post_id' not in self.kwargs:
                data_response = HttpResponse(
                    status=status.HTTP_400_BAD_REQUEST)
            else:
                deleted_post_id = self.kwargs['facebook_post_id']
                FacebookVideoPost.objects.filter(
                    post_id=deleted_post_id).delete()
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response


class DataReporter(LoginRequiredMixin, TemplateView):
    login_url = 'user/login'
    redirect_field_name = 'redirect_to'

    # Constants for report types
    SHOPIFY_STORE_REPORT = 'shopify-store-report'
    SHOPBASE_STORE_REPORT = 'shopbase-store-report'
    FACEBOOK_PAGE_REPORT = 'facebook-page-report'
    FACEBOOK_POST_REPORT = 'facebook-post-report'

    @transaction.atomic
    def _export_shopify_store_report(self, request):
        try:
            # Create a CSV response
            data_response = HttpResponse(content_type='text/csv')
            writer = csv.writer(data_response)
            writer.writerow(['Shopify Store', 'Old Domain', 'Status'])

            # Fetch Shopify store data from the database
            shopify_stores = ShopifyStore.objects.all().values_list(
                'store_url', 'old_domain', 'store_status')

            # Process the data to add 'Working' or 'Not Working' based on status
            shopify_stores = [(shopify_store[0], shopify_store[1], 'Working' if shopify_store[-1]
                               else 'Not Working') for shopify_store in shopify_stores]

            # Write data to the CSV
            for shopify_store in shopify_stores:
                writer.writerow(shopify_store)

            # Set the response filename
            data_response['Content-Disposition'] = 'attachment; filename="shopify_store_report.csv"'
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @transaction.atomic
    def _export_shopbase_store_report(self, request):
        try:
            data_response = HttpResponse(content_type='text/csv')
            writer = csv.writer(data_response)
            writer.writerow(['ShopBase Store', 'Old Domain', 'Status'])

            # Fetch ShopBase store data from the database
            shopbase_stores = ShopifyStore.objects.filter(category='shopbase').values_list(
                'store_url', 'old_domain', 'store_status')

            shopbase_stores = [(shopbase_store[0], shopbase_store[1], 'Working' if shopbase_store[-1]
                                else 'Not Working') for shopbase_store in shopbase_stores]

            for shopbase_store in shopbase_stores:
                writer.writerow(shopbase_store)

            data_response['Content-Disposition'] = 'attachment; filename="shopbase_store_report.csv"'
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @transaction.atomic
    def _export_facebook_page_report(self, request):
        try:
            data_response = HttpResponse(content_type='text/csv')
            writer = csv.writer(data_response)
            writer.writerow(['Facebook Page', 'Status'])

            # Fetch Facebook page data from the database
            facebook_pages = FacebookPage.objects.all().values_list(
                'page_url', 'page_status')

            facebook_pages = [(facebook_page[0], 'Working' if facebook_page[-1]
                               else 'Not Working') for facebook_page in facebook_pages]

            for facebook_page in facebook_pages:
                writer.writerow(facebook_page)

            data_response['Content-Disposition'] = 'attachment; filename="facebook_page_report.csv"'
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @transaction.atomic
    def _export_facebook_post_report(self, request):
        try:
            data_response = HttpResponse(content_type='text/csv')
            writer = csv.writer(data_response)
            writer.writerow(['Facebook Video Post', 'Facebook Page', 'Status'])

            # Fetch Facebook post data from the database
            facebook_posts = FacebookVideoPost.objects.all().values_list(
                'post_url', 'facebook_page_url', 'post_status')

            facebook_posts = [(facebook_post[0], facebook_post[1], 'Working' if facebook_post[-1]
                               else 'Not Working') for facebook_post in facebook_posts]

            for facebook_post in facebook_posts:
                writer.writerow(facebook_post)

            data_response['Content-Disposition'] = 'attachment; filename="facebook_post_report.csv"'
        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response

    @transaction.atomic
    def get(self, request, *args, **kwargs):
        try:
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
            report_type = request.GET.get('report_type')

            # Create a mapping of report types to methods
            report_type_to_method = {
                self.SHOPIFY_STORE_REPORT: self._export_shopify_store_report,
                self.SHOPBASE_STORE_REPORT: self._export_shopbase_store_report,
                self.FACEBOOK_PAGE_REPORT: self._export_facebook_page_report,
                self.FACEBOOK_POST_REPORT: self._export_facebook_post_report,
            }

            # Check if the report_type is valid
            if report_type in report_type_to_method:
                data_response = report_type_to_method[report_type](request)

        except Exception as e:
            print(e)
            data_response = HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        finally:
            return data_response
