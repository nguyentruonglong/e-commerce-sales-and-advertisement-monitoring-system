from django.apps import AppConfig


class BaseAppConfig(AppConfig):
    name = 'react_django_template'
