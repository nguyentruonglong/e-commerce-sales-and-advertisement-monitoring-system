import os
import environ
import dj_database_url

from pathlib import Path
from .settings import *

env = environ.Env()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Define the path to the .env_production file using pathlib
# This ensures that the path is in the correct format for both Windows and Ubuntu
env_file_path = Path(BASE_DIR).parent / 'config' / '.env_production'

env.read_env(str(env_file_path))  # Convert the Path object to a string

# Set DEBUG to False by default
DEBUG = env.bool("DEBUG", False)

# Define ALLOWED_HOSTS from the environment variable or a default
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=["*"])

# Define SECRET_KEY from the environment variable or a default
SECRET_KEY = env("SECRET_KEY", default="O_{zsZ16[tx-9W6tF&agz};B_3s1&Kn&,)4kRsqA)$(lsn'+LL")

SIMPLE_JWT = {
    # Adjust the token lifetime as needed
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=60),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
    'SLIDING_TOKEN_LIFETIME': timedelta(days=7),
    'SLIDING_TOKEN_REFRESH_LIFETIME_LAMBDA': lambda dt: dt + timedelta(days=1),
    'SLIDING_TOKEN_LIFETIME_LAMBDA': lambda dt: dt + timedelta(days=7),
    'SIGNING_KEY': SECRET_KEY,
}

CORS_ALLOWED_ORIGINS = [
    "http://localhost:8000",
    "http://127.0.0.1:8000",
    "http://0.0.0.0:80",
]

CORS_ALLOW_METHODS = (
    "DELETE",
    "GET",
    "OPTIONS",
    "PATCH",
    "POST",
    "PUT",
)

# Configure the database settings
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env("DATABASE_NAME"),
        "HOST": env("DATABASE_HOST"),
        "PORT": env("DATABASE_PORT"),
        "USER": env("DATABASE_USER"),
        "PASSWORD": env("DATABASE_PASSWORD"),
        "CONN_MAX_AGE": env("DATABASE_CONN_MAX_AGE", default=600),
    }
}

# Configure the CACHES setting to use Redis
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "ecommerce_cache",
    },
    "redis": {
        "BACKEND": "django_redis.cache.RedisCache",
        # Update the location to use the service name defined in Docker Compose
        "LOCATION": "redis://redis:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
    },
}

# Additional security settings
# SECURE_HSTS_SECONDS = 31536000  # 1 year
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True
# SECURE_HSTS_PRELOAD = True
# SECURE_CONTENT_TYPE_NOSNIFF = True
# SECURE_BROWSER_XSS_FILTER = True
# SESSION_COOKIE_SECURE = True
# SESSION_COOKIE_HTTPONLY = True
# CSRF_COOKIE_SECURE = True

# Ensure Django is aware that it's being proxied
# USE_X_FORWARDED_HOST = True
# SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# Use a secure session cookie for Django
# SESSION_COOKIE_SECURE = True

# Enable a Content Security Policy header
# CSP_DEFAULT_SRC = ("'self'",)

# Add Elasticsearch configuration
ELASTICSEARCH_DSL = {
    'default': {
        'hosts': env("ELASTICSEARCH_URL", default="http://localhost:9200"),
    },
}

# Other security settings can be added based on your project's requirements

# Configure gunicorn settings
# GUNICORN_WORKERS = env.int("GUNICORN_WORKERS", 64)
# GUNICORN_THREADS = env.int("GUNICORN_THREADS", 2)
# GUNICORN_TIMEOUT = env.int("GUNICORN_TIMEOUT", 1000)
# GUNICORN_BIND = env("GUNICORN_BIND", "0.0.0.0:8000")

# Set the path for gunicorn to find your application
# GUNICORN_PYTHONPATH = "/code/src_api"

# Define the command to run gunicorn
# GUNICORN_CMD = [
#     "gunicorn",
#     "--bind",
#     GUNICORN_BIND,
#     "--workers",
#     str(GUNICORN_WORKERS),
#     "--threads",
#     str(GUNICORN_THREADS),
#     "-t",
#     str(GUNICORN_TIMEOUT),
#     "--env",
#     "DJANGO_SETTINGS_MODULE=ecommerce_monitoring_system.settings_production",
#     "--pythonpath",
#     GUNICORN_PYTHONPATH,
#     "ecommerce_monitoring_system.wsgi:application",
# ]

# Configure GUNICORN_CMD as a single space-separated string for the command
# GUNICORN_CMD_STR = " ".join(GUNICORN_CMD)

# Update the Dockerfile to use the GUNICORN_CMD_STR
# CMD = ["/bin/bash", "-c", GUNICORN_CMD_STR]
