"""
ASGI config for ecommerce_monitoring_system project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

# Get the value of DJANGO_SETTINGS_MODULE from environment variables
settings_module = os.environ.get('DJANGO_SETTINGS_MODULE')

if settings_module:
    # The value of DJANGO_SETTINGS_MODULE is set, use it
    os.environ['DJANGO_SETTINGS_MODULE'] = settings_module
else:
    # The value is not set, provide a default value here
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ecommerce_monitoring_system.settings_local')

application = get_asgi_application()
