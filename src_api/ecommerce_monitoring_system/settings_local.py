import os
import environ

from pathlib import Path
from .settings import *


env = environ.Env()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Define the path to the .env_production file using pathlib
# This ensures that the path is in the correct format for both Windows and Ubuntu
env_file_path = Path(BASE_DIR).parent / 'config' / '.env_local'

env.read_env(str(env_file_path))  # Convert the Path object to a string


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

SIMPLE_JWT = {
    # Adjust the token lifetime as needed
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=60),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
    'SLIDING_TOKEN_LIFETIME': timedelta(days=7),
    'SLIDING_TOKEN_REFRESH_LIFETIME_LAMBDA': lambda dt: dt + timedelta(days=1),
    'SLIDING_TOKEN_LIFETIME_LAMBDA': lambda dt: dt + timedelta(days=7),
    'SIGNING_KEY': SECRET_KEY,
}

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'CONN_MAX_AGE': 60 * 10,
    }
}


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': '127.0.0.1,localhost',
    }
}


ELASTICSEARCH_DSL = {
    'default': {
        'hosts': '{}:{}'.format(env('ELASTICSEARCH_HOST'), env('ELASTICSEARCH_PORT'))
    },
}