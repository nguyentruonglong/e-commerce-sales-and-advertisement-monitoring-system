# Import required modules and classes
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from django.urls import include, re_path

# Import RedirectView, csrf_exempt for later use
from django.views.generic.base import RedirectView
from django.views.decorators.csrf import csrf_exempt

# Import Swagger documentation libraries
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

# Import URL patterns for React Django templates
from react_django_template.urls import urlpatterns as react_django_templates

# Import API URL patterns for different versions
from versioned.v1.router import urlpatterns as api_v1
from versioned.v2.router import urlpatterns as api_v2

# Define API schema for Swagger documentation
schema_view = get_schema_view(
    openapi.Info(
        title="E-COMMERCE SALES AND ADVERTISEMENT MONITORING SYSTEM API",
        default_version='v1',
        description="",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    # Include React Django template URL patterns
    path('', include(react_django_templates)),

    # Include the Django admin site
    path('admin/', admin.site.urls),

    # Include Django Rest Framework authentication URLs
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Include API URL patterns for version 1
    re_path(r'^api/v1/', include(api_v1)),

    # Include API URL patterns for version 2
    re_path(r'^api/v2/', include(api_v2)),

    # Include URL patterns for API documentation in JSON and UI formats
    re_path(r'^api/docs(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^api/docs/$', schema_view.with_ui('swagger',
                                                cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^api/redoc/$', schema_view.with_ui('redoc',
                                                 cache_timeout=0), name='schema-redoc'),

    # If there is an invalid request, redirect to the home page
    # (This line is commented out)
    # re_path(r'^.*$', RedirectView.as_view(url='/', permanent=False)),
]
