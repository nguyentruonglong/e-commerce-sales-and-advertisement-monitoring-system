# E-commerce Sales and Advertisement Monitoring System

### Table of Contents

- [E-commerce Sales and Advertisement Monitoring System](#e-commerce-sales-and-advertisement-monitoring-system)
    - [Table of Contents](#table-of-contents)
  - [Version 2.0](#version-20)
    - [Installation](#installation)
  - [Version 1.0](#version-10)
    - [System Configuration Setup and Installation](#system-configuration-setup-and-installation)
      - [Create SWAP Space for Ubuntu 18.04 VPS with 4GB RAM](#create-swap-space-for-ubuntu-1804-vps-with-4gb-ram)
      - [Install Python 3 on Ubuntu 18.04 LTS](#install-python-3-on-ubuntu-1804-lts)
      - [Install the necessary dependencies](#install-the-necessary-dependencies)
      - [Run the Web App](#run-the-web-app)
    - [System Description](#system-description)
    - [System Design](#system-design)
      - [Entity Relationship Diagram (ERD) Design for the Database](#entity-relationship-diagram-erd-design-for-the-database)
      - [Mapping the Entity Relationship Diagram to the Relational Database Model](#mapping-the-entity-relationship-diagram-to-the-relational-database-model)
    - [My Solution Approach for Building Version 1.0 of the System](#my-solution-approach-for-building-version-10-of-the-system)
      - [Resource Efficiency](#resource-efficiency)
      - [Data Management and Query Optimization](#data-management-and-query-optimization)
      - [Activity and Error Logging](#activity-and-error-logging)
      - [Docker Simplification](#docker-simplification)
      - [Front-End Integration](#front-end-integration)
    - [REST API Design](#rest-api-design)
      - [Visual Summary via Swagger](#visual-summary-via-swagger)
      - [Authentication Mechanism](#authentication-mechanism)
        - [CSRF Protection](#csrf-protection)
        - [Cookies](#cookies)
    - [Description and Examples of Some APIs](#description-and-examples-of-some-apis)
      - [Retrieve Basic Information of Shopify Stores](#retrieve-basic-information-of-shopify-stores)
        - [GET Endpoint](#get-endpoint)
        - [Example Request](#example-request)
        - [Example Response](#example-response)
      - [Retrieve Information About Shopify Products](#retrieve-information-about-shopify-products)
        - [GET Endpoint](#get-endpoint-1)
        - [Example Request](#example-request-1)
        - [Example Response](#example-response-1)
        - [Example Request](#example-request-2)
        - [Example Response](#example-response-2)
        - [Example Request](#example-request-3)
        - [Example Response](#example-response-3)
      - [Update or Insert Shopify Product Information](#update-or-insert-shopify-product-information)
        - [POST Endpoint](#post-endpoint)
        - [Example Request](#example-request-4)
        - [Example Response](#example-response-4)
      - [Update Shopify Product Saved Status](#update-shopify-product-saved-status)
        - [PATCH Endpoint](#patch-endpoint)
        - [Example Request](#example-request-5)
        - [Example Response](#example-response-5)
      - [Retrieve Daily Sales Chart for a Product](#retrieve-daily-sales-chart-for-a-product)
        - [GET Endpoint](#get-endpoint-2)
        - [Example Request](#example-request-6)
        - [Example Response](#example-response-6)
      - [Retrieve Basic Information of Facebook Pages](#retrieve-basic-information-of-facebook-pages)
        - [GET Endpoint](#get-endpoint-3)
        - [Example Request](#example-request-7)
        - [Example Response](#example-response-7)
      - [Retrieve Information about Facebook Ads](#retrieve-information-about-facebook-ads)
        - [GET Endpoint](#get-endpoint-4)
        - [Example Request](#example-request-8)
        - [Example Response](#example-response-8)
      - [Insert or Update Facebook Ads Information](#insert-or-update-facebook-ads-information)
        - [POST Endpoint](#post-endpoint-1)
        - [Example Request](#example-request-9)
        - [Example Response](#example-response-9)
      - [Update Facebook Ad Saved Status](#update-facebook-ad-saved-status)
        - [PATCH Endpoint](#patch-endpoint-1)
        - [Example Request](#example-request-10)
        - [Example Response](#example-response-10)
      - [Retrieve Basic Information of Facebook Video Posts](#retrieve-basic-information-of-facebook-video-posts)
        - [GET Endpoint](#get-endpoint-5)
        - [Example Request](#example-request-11)
        - [Example Response](#example-response-11)
      - [Insert or Update Facebook Video Posts](#insert-or-update-facebook-video-posts)
        - [POST Endpoint](#post-endpoint-2)
        - [Example Request](#example-request-12)
        - [Example Response](#example-response-12)
      - [Video Posts Analytics](#video-posts-analytics)
        - [GET Endpoint](#get-endpoint-6)
        - [Example Request](#example-request-13)
        - [Example Response](#example-response-13)
      - [Update Video Post Saved Status](#update-video-post-saved-status)
        - [PATCH Endpoint](#patch-endpoint-2)
        - [Example Request](#example-request-14)
        - [Example Response](#example-response-14)
      - [Upload Data to Server](#upload-data-to-server)
        - [POST Endpoint](#post-endpoint-3)
        - [Example Request](#example-request-15)
        - [Example Response](#example-response-15)
    - [Demo of Some Key Features](#demo-of-some-key-features)

## Version 2.0

### Installation

Run the following commands to install using Docker:

```shell
docker-compose build
docker-compose -f docker-compose.yml up
# docker-compose up -d --force-recreate
```

View activity logs with the following command:

```shell
docker-compose logs -f
```

<figure>
    <center>
        <img src="docs/images/Screenshot 24-08-2021 01-01-18.png">
        <figcaption><i></i></figcaption>
    </center>
</figure>


## Version 1.0

### System Configuration Setup and Installation

#### Create SWAP Space for Ubuntu 18.04 VPS with 4GB RAM

```shell
sudo dd if=/dev/zero of=/swapfile bs=1024 count=8192k
mkswap /swapfile
swapon /swapfile
echo /swapfile none swap defaults 0 0 >> /etc/fstab
chown root:root /swapfile
chmod 0600 /swapfile
sudo reboot
```

#### Install Python 3 on Ubuntu 18.04 LTS

```shell
sudo apt update
sudo apt-get install libpq-dev python3-dev
sudo apt-get install python-dev default-libmysqlclient-dev
sudo apt install python3-pip
```

#### Install the necessary dependencies

```shell
sudo pip3 install -r requirements.txt
```

```shell
pip3 install whitenoise
```

Add to <span>settings.py</span>:

```python
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
```

```python
MIDDLEWARE = [
  'whitenoise.middleware.WhiteNoiseMiddleware',
]
```

#### Run the Web App

```shell
export DJANGO_SETTINGS_MODULE=ecommerce_monitoring_system.settings_production && gunicorn --bind=0.0.0.0:80 --workers=64 -t 1000 --timeout 2000 --pythonpath "ecommerce_monitoring_system/src_api" "ecommerce_monitoring_system.wsgi:application"
```

### System Description

The customer is an online sales team using the Drop Shipping model and needs to collect and analyze sales activities, sales figures, and new products from business competitors on various Shopify Stores of interest. 

The customer also needs to monitor and analyze product advertising activities on Facebook from the Facebook Pages they gather. The customer wants to build a tool to analyze and aggregate information from up to 1000 Shopify Stores with an estimated total of nearly 500,000 products. In addition, the number of Facebook Pages and Facebook Video Posts in advertising campaigns to be monitored and analyzed also reaches thousands.

The tool has the following functionalities as per the customer's requirements:

- **Collecting Information and Monitoring Sales Activities on Shopify Stores:** 
  - Collecting basic information for each Shopify Store, including the Store's title and its primary currency.
  - Collecting basic information for all products on each Shopify Store, including the Product ID, product URL, thumbnail URL, product name, creation time of the product, the most recent sales time of the product, product price, and the best selling rank of the product within a store.
  - Scanning and collecting data from the list of Shopify Stores every 3 hours.
  - Sorting and filtering functions for all collected products, including sorting by creation time, trending products (based on recent sales within 3 days), and the most recent sales time.
  - Filtering products based on their creation time, such as 1 day, 3 days, 7 days, 14 days, 30 days, 90 days, and all time.
  - Filtering products by individual stores.
  - User interface consisting of a menu, filters, and items. Each item represents a product and contains relevant collected and calculated information, including the product's thumbnail, time since creation, and the most recent sales. When hovering over an item, the product name is displayed. Each item has a button with the Store Title and a link to the Shopify Store's homepage with the respective product.
  - Loading and displaying information for 50 products at a time in a 5-column and 10-row layout.
  - Calculating sales revenue for each product with a rough estimate. Data is updated every 3 hours, and if the latest sales time differs from the stored value in the database, the new sales time is added to the database.
  - Drawing sales charts for each product for the past 7 days based on the collected purchase history in the database.

- **Collecting, Monitoring, and Analyzing Advertising Activities on Facebook Pages:**
  - Extracting Page ID and Page Name from Facebook Page URLs.
  - Extracting information about advertising posts on each Facebook Page through the Facebook Ad Library and the extracted Page ID. The information includes Advertising ID, start time of advertising, URL of embedded video, URL of thumbnail, URL of the advertised product, checking if the URL belongs to a Shopify Store, creation time of the advertised product, the most recent sales time, and product price.
  - Scanning and collecting data from the list of Facebook Pages every 24 hours.
  - User interface for this function with a menu, filters, and items. Each item displays information about the collected advertising posts, including the embedded video thumbnail, URL of the advertised product, time since creation, and the most recent sales.
  - Clicking on an item redirects to the URL of the advertised product in a new tab.
  - Sorting and filtering functions for all collected advertising posts, including sorting by the start time of advertising and the most recent sales time, and filtering by the start time of advertising (1 day, 3 days, 7 days, 14 days, 30 days, 90 days, and all time).

- **Collecting and Monitoring Facebook Video Post Activities:**
  - Extracting interaction data, including reactions, comments, and shares, for each video post daily. These video posts are used in product advertising campaigns.
  - Extracting video title, post creation date, Facebook Page name, video URL, and video thumbnail.
  - Scanning and collecting data from the list of Facebook Video Posts every 24 hours.
  - User interface for this function includes a menu, filters, and items. Each item displays information about a video post, including the embedded video thumbnail, video URL, and interaction changes between the current day and the previous day.
  - A chart shows trends in reactions, comments, and shares in the last 7 days when hovering over an item.
  - Sorting and filtering functions for all collected video posts, including sorting by post creation time and trending posts (based on share changes compared to the previous 3 days) and by the current share count.

- **Managing Lists of Objects:**
  - Three separate tabs for managing lists of Shopify Stores, Facebook Pages, and Facebook Video Posts.
  - Functionality for editing, deleting, and marking objects with checkboxes.
  - Option to delete multiple objects with checkboxes.
  - Export feature for all relevant information of the objects in CSV format.

- **User Authentication:**
  - Users are required to log in before using the tool, with the ability to reset their password if necessary.

Additional Changes:
- Changed the scrape update frequency of Shopify products from every 3 hours to every 15 minutes.
- Removed the function to scrape best-selling data from Shopify and replaced it with estimated best-selling indicators for 1, 3, 7, 14, 30, and 90 days.
- Added the ability to view best-selling products for each store based on the calculated estimates.
- Integrated data collection and analysis functions for ShopBase Stores, similar to Shopify Stores.
- Made API modifications and added features to the best-selling tab interface.
- Updated the design for the Facebook Ads statistics and analysis page.


### System Design

<figure>
    <center>
        <img src="https://lh3.googleusercontent.com/pw/ACtC-3dYHMBc-wCxDRLp7BGxx_nRYik4Zuto1BQotrKiXE07aa2LjOloMk0nahywpKJv9J7iu4IRpwos49m8s39PQEk76ju6LzPsPZFYi-V3P86Mn_SC7_KnU_dBJx64Ka69AXJwcArGa-mwOlqBUEQr-rc=w1168-h657-no?authuser=0">
        <figcaption style="text-align:center"><i>The system has an architecture similar to the Microservice Architecture Pattern combined with API Gateway</i></figcaption>
    </center>
</figure>

The system combines various independent functionalities, and these functions are implemented on different distributed VPSs:

<ul>
    <li>A cluster of Windows Server 2016 VPSs is used to deploy modules for collecting product data from various Shopify Stores. See a summary demo <a href="https://drive.google.com/file/d/1k0UUujZEC6e1e3RUzM948zQmk5-MGU67/view" target="_blank">here</a>.
    </li>
    <li>A cluster of Windows Server 2016 VPSs is used to deploy modules for collecting advertisement data from various Facebook Pages. See a summary demo <a href="https://drive.google.com/file/d/1pkJXO6PCERk0eqrII5FhD-NltIzdqpaL/view" target="_blank">here</a>.
    </li>
    <li>A cluster of Windows Server 2016 VPSs is used to deploy modules for collecting data from Facebook Video Posts in ad campaigns.</li>
    <li>Multiple Windows Server 2016 VPSs work in parallel to achieve a rapid completion of tasks in a fixed short cycle of a few hours for a large number of objects that need data collection.</li>
    <li>1 Ubuntu 18.04 LTS VPS is used to deploy the Database Server, Nginx Web Server, various backend modules, and aggregate data collected from other VPSs, functioning like an API Gateway. See a summary demo <a href="https://drive.google.com/file/d/1tzvC9q61Be69aNRStsACMQZdtiguoQru/view" target="_blank">here</a>.
    </li>
    <li>Communication between VPSs is done through REST APIs.</li>
</ul>

The operational process of the tool can be summarized in the following main ideas:

<ul>
    <li>Customers enter or update a list of objects that need data collection into the database by uploading a file containing the list of objects on the Web App interface.</li>
    <li>The modules responsible for data collection on the distributed VPSs will send requests to the API Gateway to receive a list of objects that need data collection according to a specific cycle set for each type of object.</li>
    <li>The data collection modules will perform automated behavior simulation using a non-headless Browser Driver and send the extracted data to the Database Server through REST API requests sent to the API Gateway.</li>
    <li>The front-end uses ReactJS combined with Django template to interact with the REST API, sending requests and receiving feedback data from the back-end to display on the interface. Customers interact with the tool through the Web App interface.</li>
</ul>

#### Entity Relationship Diagram (ERD) Design for the Database

Identify entities and their attributes:

<ul>
    <li>
        <b>A Shopify Store entity has the following attributes:</b>
        <ul>
            <li><i>Store URL</i> (a key attribute representing the URL of the store)</li>
            <li><i>Website Title</i> (a single attribute representing the name of the store)</li>
            <li><i>Currency Code</i> (a single attribute representing the code of the primary currency used by the store)</li>
            <li><i>Last Crawling</i> (a single attribute representing the last update of the value collected in the latest session)</li>
        </ul>
    </li>
    <li>
        <b>A Shopify Product entity has the following attributes:</b>
        <ul>
            <li><i>Product ID</i> (a key attribute representing the ID of the product on the Shopify Store)</li>
            <li><i>Product URL</i> (a single attribute representing the product's URL)</li>
            <li><i>Product Title</i> (a single attribute representing the product's name)</li>
            <li><i>Thumbnail URL</i> (a single attribute representing the URL of the product's thumbnail image)</li>
            <li><i>Best Selling Index</i> (a single attribute representing the product's best-selling ranking within a local store)</li>
            <li><i>Created At</i> (a single attribute representing the creation time of the product)</li>
            <li><i>Updated At</i> (a single attribute representing the most recent purchase time of the product)</li>
            <li><i>Price</i> (a single attribute representing the price of the product)</li>
            <li><i>Total Sales</i> (a derived attribute representing the total number of all purchase sessions of the product)</li>
            <li><i>Total Sales Last Day</i> (a derived attribute representing the total number of product purchases on the most recent day)</li>
            <li><i>Trending Index</i> (a derived attribute representing the total number of product purchases in the last 3 days)</li>
            <li><i>Home URL</i> (a single attribute representing the URL of the Shopify Store containing the product)</li>
            <li><i>Home Title</i> (a single attribute representing the name of the Shopify Store containing the product)</li>
            <li><i>Description</i> (a single attribute representing the product description)</li>
            <li><i>Saved</i> (a single attribute representing the note status of each product item)</li>
            <li><i>Last Saved</i> (a single attribute representing the time when the product item was last noted)</li>
            <li><i>Daily Sales Chart</i> (a derived attribute containing the coordinates of a chart representing the number of product purchases in the last 7 days)</li>
            <li><i>Purchase</i> (a multi-valued attribute representing the time of purchase for each product, with each product having multiple values recording the purchase time)</li>
        </ul>
    </li>
    <li>
        <b>A Facebook Page entity has the following attributes:</b>
        <ul>
            <li><i>Facebook Page ID</i> (a key attribute representing the ID of the Facebook Page)</li>
            <li><i>Facebook Page URL</i> (a single attribute representing the URL of the Facebook Page)</li>
            <li><i>Facebook Page Name</i> (a single attribute representing the name of the Facebook Page)</li>
            <li><i>Last Crawling</i> (a single attribute representing the last update of the value collected in the latest session)</li>
        </ul>
    </li>
    <li>
        <b>A Facebook Ad entity has the following attributes:</b>
        <ul>
            <li><i>Advertising ID</i> (a key attribute representing the ID of the Facebook Ad)</li>
            <li><i>Product URL</i> (a single attribute representing the value of the advertised product's URL)</li>
            <li><i>Started Running</i> (a single attribute representing the time when the ad started running)</li>
            <li><i>Embedded Video URL</i> (a single attribute representing the value of the video URL in the Facebook Ad)</li>
            <li><i>Embedded Video Thumbnail URL</i> (a single attribute representing the value of the video thumbnail URL in the Facebook Ad)</li>
            <li><i>Description</i> (a single attribute representing the description of the ad in the Facebook Ad)</li>
            <li><i>Created At</i> (a single attribute representing the time when the advertised product was created)</li>
            <li><i>Updated At</i> (a single attribute representing the most recent purchase time of the advertised product)</li>
            <li><i>Price</i> (a single attribute representing the price of the advertised product)</li>
            <li><i>Saved</i> (a single attribute representing the note status of each ad item)</li>
            <li><i>Last Saved</i> (a single attribute representing the time when the ad item was last noted)</li>
            <li><i>Facebook Page ID</i> (a single attribute representing the ID of the corresponding Facebook Page)</li>
        </ul>
    </li>
    <li>
        <b>A Facebook Video Post entity has the following attributes:</b>
        <ul>
            <li><i>Posted URL</i> (a single attribute representing the URL of the video post)</li>
            <li><i>Posted At</i> (a single attribute representing the creation time of the video post)</li>
            <li><i>Embedded Video URL</i> (a single attribute representing the video URL of the video post)</li>
            <li><i>Embedded Thumbnail Video URL</i> (a single attribute representing the thumbnail URL of the video post)</li>
            <li><i>Facebook Page Name</i> (a single attribute representing the name of the Facebook Page containing the video post)</li>
            <li><i>Video Title</i> (a single attribute representing the title of the video post)</li>
            <li><i>Trending Index</i> (a derived attribute representing the total number of shares of the video post in the last 3 days)</li>
            <li><i>Last Crawling</i> (a single attribute representing the last update of the value collected in the latest session)</li>
            <li><i>Engagement</i> (a multi-valued attribute representing the number of reactions, comments, and shares of a video post at a fixed time each day)</li>
            <li><i>Engagement Chart</i> (a derived attribute containing coordinates for 3 lines on a chart representing the values of the number of reactions, comments, and shares in the last 7 days)</li>
        </ul>
    </li>
</ul>

Establish relationships between entities:

<ul>
    <li>A Shopify Store may have zero or multiple Shopify Products.</li>
    <li>A Shopify Product belongs to only one Shopify Store.</li>
    <li>A Facebook Page may have zero or multiple Running Ads.</li>
    <li>A Running Ad belongs to only one Facebook Page.</li>
</ul>

<figure>
    <center>
        <img src="https://live.staticflickr.com/65535/50045997823_9c47a28520_k_d.jpg">
        <figcaption style="text-align:center"><i>Entity Relationship Diagram (ERD) of the database designed based on the customer's descriptions</i></figcaption>
    </center>
</figure>

#### Mapping the Entity Relationship Diagram to the Relational Database Model

<figure>
    <center>
        <img src="https://live.staticflickr.com/65535/50045992848_dd258941a0_k_d.jpg">
        <figcaption style="text-align:center"><i>The Relational Database Model mapped from the ERD with some additional customizations</i></figcaption>
    </center>
</figure>


### My Solution Approach for Building Version 1.0 of the System

In crafting the v1 version of the E-commerce Sales and Advertisement Monitoring System, my foremost principle is to maintain simplicity, resource efficiency, and adaptability. This approach ensures that the system is accessible, cost-effective, and capable of handling substantial data volumes.

#### Resource Efficiency
A paramount consideration is resource utilization. I aim to ensure that the system's backend operates seamlessly with just one Ubuntu 18.04 LTS VPS, equipped with 4GB of RAM. This resource allocation is optimized to manage the data generated by over 500,000 Shopify products and additional system-generated data. The focus here is on efficient resource management rather than relying on resource-intensive solutions.

#### Data Management and Query Optimization
Rather than incorporating resource-demanding servers like ElasticSearch or Redis for query optimization, I adopt an alternative approach. The emphasis is on optimizing data insertion and enhancing complex data querying using Django's inherent libraries. I also cached APIs that query large volumes of data with complex logic in a short time on the client-side browser side instead of on the server-side. This strategy minimizes the need for additional resources, thus reducing operational costs.

#### Activity and Error Logging
While Kibana offers extensive functionalities for recording system activities and errors, I opted for a more cost-effective approach. To streamline activity and error logging without incurring extra costs, the system leverages Django's built-in error logging functionality. This method simplifies the process and ensures that all crucial system activities and errors are recorded efficiently.

#### Docker Simplification
While Docker offers advantages in terms of deployment and scalability, its utilization increases system requirements and costs. Therefore, in this version, I opt for manual installation of system components. This approach provides better control over resource usage and system behavior while keeping costs in check.

#### Front-End Integration
To avoid additional expenses associated with a separate server for the ReactJS project, the system integrates the ReactJS code files, which are generated from the webpack portion of the project, directly into the Django template. This integration provides a cost-effective and efficient way to render the user interface without compromising system performance.

This solution aligns with the overarching principles of simplicity, resource efficiency, and adaptability. It focuses on optimizing data management, reducing costs, and enhancing system performance, making the E-commerce Sales and Advertisement Monitoring System a practical and sustainable solution for monitoring and analyzing e-commerce activities.


### REST API Design

#### Visual Summary via Swagger

<figure>
    <center>
        <img src="docs/images/FireShot Capture 003 - E-COMMERCE SALES AND ADVERTISEMENT MONITORING SYSTEM API - localhost.png">
        <figcaption style="text-align:center"><i>E-commerce Sales and Advertisement Monitoring System API v1 docs</i></figcaption>
    </center>
</figure>

Principles of REST API Design:

<ul>
    <li>Flexible: The API must be flexible, easy to customize, and expandable.</li>
    <li>Self-documenting: It should be easy to understand how the API operates in a transparent manner.</li>
    <li>Clear and concise error messages: Predicting errors clearly when the system encounters problems should be straightforward.</li>
</ul>

#### Authentication Mechanism

The v1 version of the system employs a robust authentication mechanism to secure requests from the front-end, which is developed using ReactJS combined with Django templates. The primary mechanisms utilized in this system are Cross-Site Request Forgery (CSRF) protection and cookies.

##### CSRF Protection

The system leverages Django's built-in CSRF protection using the `csrf_exempt` decorator from `django.views.decorators.csrf`. CSRF protection is crucial for preventing Cross-Site Request Forgery attacks. Here's how it works:

- When a user logs in or interacts with the system, Django generates a unique token known as a CSRF token.
- This token is included in forms and requests sent to the server.
- When the server receives a request, it checks if the CSRF token in the request matches the token associated with the user's session.
- If the tokens match, the request is processed; otherwise, it is rejected as potentially malicious.

The use of CSRF protection enhances the security of the system by ensuring that requests originate from legitimate sources and are not forged by malicious actors.

##### Cookies

Cookies play a fundamental role in user authentication in the v1 version of the system. The system uses cookies for:

- **Authentication**: Cookies are set when a user logs in, storing authentication information. This allows users to remain authenticated across multiple requests without having to re-enter their credentials.

- **Session Management**: Cookies help manage user sessions, enabling the system to recognize and remember users between interactions.


### Description and Examples of Some APIs

The REST API of features related to Shopify Store is listed as follows:

#### Retrieve Basic Information of Shopify Stores

##### GET Endpoint
- **GET /api/v1/shopify-stores**

This endpoint allows you to retrieve basic information about all stores stored in the database. Modules with data scraping functionality can send this request to receive a list of objects to collect data.

##### Example Request

```bash
    curl -X GET "/api/v1/shopify-stores"
```

##### Example Response

An example response for this request:

```json
    [
        {
            "store_url": "https://icoolstone.com",
            "website_title": "ICOOLSTONE",
            "last_crawling": "2020-05-15T03:00:00+07:00",
            "currency_code": "USD",
            "category": "Unknown Category"
        },
        {
            "store_url": "https://1towish.com",
            "website_title": "1towish",
            "last_crawling": "2020-06-23T18:00:00.099721+07:00",
            "currency_code": "USD",
            "category": "Unknown Category"
        },
        {
            "store_url": "https://365fridays.com",
            "website_title": "365Fridays com Over 99 000 Happy Customer ",
            "last_crawling": "2020-06-23T18:00:00.099721+07:00",
            "currency_code": "USD",
            "category": "Unknown Category"
        },
        {
            "store_url": "https://5to9vivid.com",
            "website_title": "5to9vivid",
            "last_crawling": "2020-06-23T18:00:00.099721+07:00",
            "currency_code": "USD",
            "category": "Unknown Category"
        }
    ]
```

In this example, a cURL request is made to the <i>GET /api/v1/shopify-stores</i> endpoint, which returns a JSON response containing basic information about the Shopify stores in the database.

#### Retrieve Information About Shopify Products

##### GET Endpoint
- **GET /api/v1/shopify-products**

This endpoint allows you to retrieve information about Shopify Products based on orders and filters, making it a powerful tool for analyzing e-commerce product data. If the request does not include additional parameters, the server-side will use the default parameters set.

##### Example Request

```bash
curl -X GET "/api/v1/shopify-products?tab=all-products&period=all-time&sort_by=published-time&page=1"
```

- Retrieve information about Shopify Products with specific parameters set:
  - `tab` is set to "all-products".
  - `period` is set to "all-time" (fetching all products).
  - `sort_by` is set to "published-time" (sorting products in descending order of creation time, with the most recently created products appearing first).
  - `page` is set to 1 (corresponding to loading the first 50 products).


##### Example Response

An example response for this request:

```json
{
    "count": 231035,
    "next": "http://nguyentruonglong.com/api/v1/shopify-products?page=2&period=all-time&sort_by=published-time&tab=best-selling",
    "previous": null,
    "results": [
        {
            "product_id": "4463673835580",
            "website_title": "fogsale",
            "last_crawling": "2020-06-23T09:00:00.154399+07:00",
            "currency_code": "USD",
            "product_title": "WIRELESS WIFI CAMERA WITH SENSORI NIGHT VISION",
            "price": 29.99,
            "created_at": "2020-02-18T10:11:20+07:00",
            "updated_at": "2020-06-23T09:56:03+07:00",
            "best_selling_index": 1,
            "trending_index": 13,
            "total_sales": 366,
            "total_sales_last_day": 4,
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0269/8867/5132/products/kosarica-1.jpg?v=1581995501",
            "product_url": "https://fogsale.com/products/wireless-wifi-camera-with-sensori-night-vision",
            "saved": false,
            "last_saved": "2020-06-22T17:12:03+07:00",
            "description": "",
            "daily_sales_chart": "{\"1\": 5, \"2\": 9, \"3\": 7, \"4\": 5, \"5\": 1, \"6\": 8, \"7\": 4}",
            "home_url": "https://fogsale.com"
        },
        {
            "product_id": "4521489072225",
            "website_title": "SmarToLife Smartolife",
            "last_crawling": "2020-06-23T12:00:00.793144+07:00",
            "currency_code": "USD",
            "product_title": " Last 1 Day Promotion 80 OFF Ceramic Tourmaline Ionic Flat Iron Hair Straightener",
            "price": 29.98,
            "created_at": "2020-02-29T01:16:33+07:00",
            "updated_at": "2020-06-23T13:00:47+07:00",
            "best_selling_index": 1,
            "trending_index": 21,
            "total_sales": 353,
            "total_sales_last_day": 5,
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0270/6119/0753/products/st1.jpg?v=1586688725",
            "product_url": "https://smartolife.com/products/straightener",
            "saved": true,
            "last_saved": "2020-05-03T21:34:24+07:00",
            "description": "",
            "daily_sales_chart": "{\"1\": 5, \"2\": 7, \"3\": 8, \"4\": 7, \"5\": 8, \"6\": 8, \"7\": 5}",
            "home_url": "https://smartolife.com"
        }
    ]
}
```

If an error occurs on the server-side when processing the request, the response will include a response code and a specific error message, for example:

```json
{
    "error": "Exception happened during processing of request from ('127.0.0.1', 51937)"
}
```

##### Example Request

```bash
curl -X GET "/api/v1/shopify-products?tab=saved&page=1&session=1592940600189"
```

- Retrieve information about Shopify Products with specific parameters set:
    - `tab` is set to "saved".
    - `page` is set to 1 (corresponding to the first load displaying 50 products).
    - `session` is set to 1592940600189, indicating a specific session.

Here's another way to use <i>GET /api/v1/shopify-products</i>. You can fetch a list of products from Shopify Stores that users have shown interest in and saved to a smaller list. This makes it easier to analyze specific products, especially when dealing with a vast inventory of hundreds of thousands.

##### Example Response

An example response for this request:

```json
{
    "count": 29,
    "next": null,
    "previous": null,
    "results": [
        {
            "product_id": "4585257697343",
            "website_title": "Pudding Candy focus on making your life easy beautiful",
            "last_crawling": "2020-06-24T00:00:00.286326+07:00",
            "currency_code": "USD",
            "product_title": "Auto Darkening Welding Glasses",
            "price": 39.98,
            "created_at": "2020-04-30T10:29:42+07:00",
            "updated_at": "2020-06-23T21:46:06+07:00",
            "best_selling_index": -1,
            "trending_index": 6,
            "total_sales": 73,
            "total_sales_last_day": 0,
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0055/0535/1743/products/thumbnail_c9206344-592f-431b-b384-2278da1a2024.jpg?v=1588221327",
            "product_url": "https://puddingcandy.com/products/auto-darkening-welding-glasses",
            "saved": true,
            "last_saved": "2020-06-22T17:15:36+07:00",
            "description": "",
            "daily_sales_chart": "{\"1\": 0, \"2\": 0, \"3\": 2, \"4\": 2, \"5\": 4, \"6\": 2, \"7\": 0}",
            "home_url": "https://puddingcandy.com"
        },
        {
            "product_id": "4541618126895",
            "website_title": "SmilingScent",
            "last_crawling": "2020-06-24T00:00:00.286326+07:00",
            "currency_code": "USD",
            "product_title": "Shank Dovetail Joint Router Bit 5 pcs",
            "price": 29.97,
            "created_at": "2020-05-11T11:31:57+07:00",
            "updated_at": "2020-05-25T05:51:38+07:00",
            "best_selling_index": 69,
            "trending_index": 0,
            "total_sales": 17,
            "total_sales_last_day": 0,
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0247/8768/1327/products/20200508-SoloSoccerTrainingBelt-Thumbnails_Karl-04_11bda429-bade-4c92-9840-854920e38b42.jpg?v=1589171517",
            "product_url": "https://smilingscent.com/products/shank-dovetail-joint-router-bit-1",
            "saved": true,
            "last_saved": "2020-06-22T17:15:25+07:00",
            "description": "",
            "daily_sales_chart": "{\"1\": 0, \"2\": 0, \"3\": 0, \"4\": 0, \"5\": 0, \"6\": 0, \"7\": 0}",
            "home_url": "https://smilingscent.com"
        }
    ]
}
```

##### Example Request

```bash
curl -X GET "/api/v1/shopify-products?store_url=https%3A%2F%2Ffogsale.com&period=all-time&sort_by=published-time&page=2"
```

- Retrieve information of Shopify Products filtered by the website with the address <a href="https://fogsale.com">https://fogsale.com</a>. In this scenario:

    - `store_url` is set to <i>https://fogsale.com</i>.
    - `period` is set to "all-time" (fetching products from all time periods).
    - `sort_by` is set to "published-time" which means products are sorted in descending order of creation time, with the most recently created products appearing first.
    - `page` is set to 2, corresponding to loading the next 50 products for the second time.

##### Example Response

An example response for this request:

```json
{
    "count": 134,
    "next": "http://nguyentruonglong.com/api/v1/shopify-products?page=3&period=all-time&sort_by=published-time&store_url=https%3A%2F%2Ffogsale.com",
    "previous": "http://nguyentruonglong.com/api/v1/shopify-products?period=all-time&sort_by=published-time&store_url=https%3A%2F%2Ffogsale.com",
    "results": [
        {
            "product_id": "4358040059964",
            "website_title": "fogsale",
            "last_crawling": "2020-06-24T00:00:00.422889+07:00",
            "currency_code": "USD",
            "product_title": "Cute rabbit alarm clock creative led digital household items",
            "price": 30.2,
            "created_at": "2019-11-20T14:32:51+07:00",
            "updated_at": "2020-05-03T03:55:53+07:00",
            "best_selling_index": 36,
            "trending_index": 0,
            "total_sales": 2,
            "total_sales_last_day": 0,
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0269/8867/5132/products/product-image-1189535578.jpg?v=1574235212",
            "product_url": "https://fogsale.com/products/cute-rabbit-alarm-clock-creative-led-digital-household-items",
            "saved": false,
            "last_saved": "2020-04-14T22:07:41.407167+07:00",
            "description": "",
            "daily_sales_chart": "{\"1\": 0, \"2\": 0, \"3\": 0, \"4\": 0, \"5\": 0, \"6\": 0, \"7\": 0}",
            "home_url": "https://fogsale.com"
        },
        {
            "product_id": "4358034096188",
            "website_title": "fogsale",
            "last_crawling": "2020-06-24T00:00:00.422889+07:00",
            "currency_code": "USD",
            "product_title": "Pumpkin Night Lights Halloween Party Decoration Accessories",
            "price": 4.86,
            "created_at": "2019-11-20T14:29:05+07:00",
            "updated_at": "2020-06-11T05:15:58+07:00",
            "best_selling_index": 49,
            "trending_index": 0,
            "total_sales": 2,
            "total_sales_last_day": 0,
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0269/8867/5132/products/product-image-1127742431.jpg?v=1574234950",
            "product_url": "https://fogsale.com/products/pumpkin-night-lights-halloween-party-decoration-accessories",
            "saved": false,
            "last_saved": "2020-04-14T22:07:41.416847+07:00",
            "description": "",
            "daily_sales_chart": "{\"1\": 0, \"2\": 0, \"3\": 0, \"4\": 0, \"5\": 0, \"6\": 0, \"7\": 0}",
            "home_url": "https://fogsale.com"
        }
    ]
}
```

#### Update or Insert Shopify Product Information

##### POST Endpoint
- **POST /api/v1/shopify-products**

This endpoint allows you to insert or update all information of Shopify Products in a scraped Shopify Store received from scraping module in other VPS.

##### Example Request

```shell
curl -X POST "/api/v1/shopify-products" -H "Content-Type: application/json" -d '{
    "store_url": "https://fogsale.com",
    "website_title": "fogsale",
    "currency_code": "USD",
    "last_crawling": "2020-06-22 02:10:55.318885",
    "products_info": {
        "4463673835580": {
            "product_url": "https://fogsale.com/products/wireless-wifi-camera-with-sensori-night-vision",
            "best_selling_index": 1,
            "product_id": 4463673835580,
            "product_title": "WIRELESS WIFI CAMERA WITH SENSORI NIGHT VISION",
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0269/8867/5132/products/kosarica-1.jpg?v=1581995501",
            "price": "29.99",
            "created_at": "2020-02-18 10:11:20",
            "updated_at": "2020-06-22 02:01:33"
        },
        "4483701964860": {
            "product_url": "https://fogsale.com/products/in-stock-ships-within-24-hours",
            "best_selling_index": 2,
            "product_id": 4483701964860,
            "product_title": "In Stock ships within 24 hours",
            "thumbnail_url": "https://cdn.shopify.com/s/files/1/0269/8867/5132/products/Eachine_E58_WIFI_FPV_With_Wide_Angle_HD_1080P_Camera_Hight_Hold_Mode_Foldable_Arm_RC_1400x_7249993d-49f9-44ce-8116-d9a21315cce2.jpg?v=1585536691",
            "price": "59.98",
            "created_at": "2020-03-30 09:51:31",
            "updated_at": "2020-06-21 02:50:58"
        }
    }
}'
```

##### Example Response

If a server-side error occurs while processing the request, the response will include an error code along with a specific error message, for example:

```json
{
    "error" : "IntegrityError: Insert or update on table \"shopify_products_analyzer_shopifyproduct\" violates foreign key constraint"
}
```

#### Update Shopify Product Saved Status

##### PATCH Endpoint
- **PATCH /api/v1/shopify-products**

This endpoint allows you to update the 'Saved' field value in the corresponding database table for Shopify Products. You can mark a product as 'noted' or 'unnoted' in the user interface.

##### Example Request

```shell
curl -X PATCH "/api/v1/shopify-products" -H "Content-Type: application/json" -d '{
    "product_id": "4601282002998",
    "last_saved": "2020-06-23T19:53:54+07:00"
}'
```

- Update Shopify Product 'Saved' status with specific parameters set:
    - `product_id` specifies the unique identifier of the Shopify product.
    - `last_saved` indicates the date and time of the update.


##### Example Response
If a server-side error occurs while processing the request, the response will include an error code along with a specific error message, for example:

```json
{
    "error": "String was not recognized as a valid DateTime"
}
```

#### Retrieve Daily Sales Chart for a Product

##### GET Endpoint
- **GET /api/v1/daily-sales-chart?product_id=4463673835580**

This endpoint allows you to retrieve the sales chart for the last 7 days of the product with the `product_id` value of 4463673835580 in image format .PNG.

##### Example Request

```shell
curl -X GET "/api/v1/daily-sales-chart?product_id=4463673835580"
```

- Retrieve Daily Sales Chart with specific parameters set:
    - `product_id` specifies the unique identifier of the product.

##### Example Response

An example response for this request:

<figure>
    <center>
        <img src="https://lh3.googleusercontent.com/pw/ACtC-3cykZa5NdIokr6wpHBJ7VgpOJKfO1fctHc9bIFpzWbtLZvz0sJ5Vw6cyUn3nsq8FXS7nGT0jtZKcrVKfqSFnvFpwoZ63BU8gnOJEQvapPC6HrL_hKkjpWctZvx0CxX2BLPHsRKkuwgfW6Pnnttzz54=w1233-h657-no?authuser=0">
    </center>
</figure>

If a server-side error occurs while processing the request, the response will include an error code along with a specific error message, for example:

```json
{
    "error": "RuntimeError: main thread is not in the main loop"
}
```

This API is currently no longer in use as it consumes a significant amount of server-side resources and leads to a poor user experience. Instead, the server-side responds with JSON containing chart coordinate information, and the Chart.js library on the front-end uses this information to draw the chart.


The REST API system for functions related to Facebook Ads is listed as follows:

#### Retrieve Basic Information of Facebook Pages

##### GET Endpoint
- **GET /api/v1/facebook-pages**

This endpoint allows you to retrieve basic information for all Facebook pages currently stored in the database. Modules with data scraping functionality can send this request to receive a list of objects to collect data.

##### Example Request

```bash
curl -X GET "/api/v1/facebook-pages"
```

##### Example Response

```json
[
    {
        "page_id": "100252918194879",
        "page_url": "https://www.facebook.com/Aliands-100252918194879/",
        "page_name": "Aliands",
        "last_crawling": "2020-06-23T11:00:00.045345+07:00"
    },
    {
        "page_id": "100347934967655",
        "page_url": "https://www.facebook.com/Dayupup-100347934967655/",
        "page_name": "Dayupup Product Service",
        "last_crawling": "2020-06-23T11:00:00.045345+07:00"
    },
    {
        "page_id": "100397011706533",
        "page_url": "https://www.facebook.com/Ferris-Hub-Daily-100397011706533/",
        "page_name": "Ferris Hub Daily",
        "last_crawling": "2020-06-23T11:00:00.045345+07:00"
    },
    {
        "page_id": "100441364824247",
        "page_url": "https://www.facebook.com/Daily-life-100441364824247/",
        "page_name": "Daily life E-commerce Website",
        "last_crawling": "2020-06-23T11:00:00.045345+07:00"
    }
]
```

In this example, a cURL request is made to the <i>GET /api/v1/facebook-pages</i> endpoint, which returns a JSON response containing basic information about the Facebook pages in the database.

#### Retrieve Information about Facebook Ads

##### GET Endpoint
- **GET /api/v1/facebook-ads**

This endpoint allows you to retrieve information about Facebook Ads based on orders and filters. If the request does not contain additional parameters, the server-side will use default parameters.

##### Example Request

To retrieve information about Facebook Ads with specific parameters, use the following example request:

```bash
curl -X GET "/api/v1/facebook-ads?tab=all-ads&period=90-days&sort_by=started-running&page=2"
```

- In this scenario:
    - `tab` is set to "all-ads".
    - `period` is set to "90-days" (retrieving ads with a "started running" time within the last 90 days).
    - `sort_by` is set to "started-running" (prioritizing ads that have run most recently).
    - `page` is set to 2 (corresponding to loading the second set of 50 Facebook Ads).

##### Example Response

An example response for this request:

```json
{
    "count": 213,
    "next": "http://nguyentruonglong.com/api/v1/facebook-ads?page=3&period=90-days&sort_by=started-running&tab=all-ads",
    "previous": "http://nguyentruonglong.com/api/v1/facebook-ads?period=90-days&sort_by=started-running&tab=all-ads",
    "results": [
        {
            "advertising_id": "1261389954233398",
            "page_url": "https://www.facebook.com/Gadget-Home-413127162760957/",
            "page_name": "Gadget Home",
            "last_crawling": "2020-06-23T11:00:00.045345+07:00",
            "embedded_video_url": "https://video-lga3-1.xx.fbcdn.net/v/t42.9040-2/105947707_864347824059253_7165669889212620148_n.mp4?_nc_cat=103&_nc_sid=cf96c8&_nc_oc=AQntxPtlKRc-lDkLGtRA7C5GOTUu-6rWMyth2q83DMVmdRm3YmhhfHW9ZOiXgqy_5nc&_nc_ht=video-lga3-1.xx&oh=536a564dc0632f485efa9ab9ce8e9574&oe=5EF1C72A",
            "embedded_video_thumbnail_url": "https://scontent-lga3-1.xx.fbcdn.net/v/t39.16868-6/105949316_1261389980900062_4054066997507337811_n.jpg?_nc_cat=105&_nc_sid=cf96c8&_nc_oc=AQn6N-JprBqFdoC78y55Fig_AmBdZR0qXdHsNETZgCBu-0nSBlkq1pQTGcyOi1Kw7xk&_nc_ht=scontent-lga3-1.xx&oh=733769cd4d60ffff9c599ea451abb5ac&oe=5F174D51",
            "product_url": "https://gadgetcab.com/products/cac",
            "started_running": "2020-06-22T00:00:00+07:00",
            "saved": false,
            "price": 36.99,
            "created_at": "2020-06-23T10:51:12+07:00",
            "updated_at": "2020-06-23T10:51:13+07:00",
            "last_saved": "2020-06-23T13:52:11.395849+07:00",
            "description": "",
            "page_id": "413127162760957"
        },
        {
            "advertising_id": "1636856259797837",
            "page_url": "https://www.facebook.com/Summerde-gift-462486437644644/",
            "page_name": "Summerde gift E-commerce Website",
            "last_crawling": "2020-06-23T11:00:00.045345+07:00",
            "embedded_video_url": "https://video-lga3-1.xx.fbcdn.net/v/t42.9040-2/106055879_283549202767354_526254717757563351_n.mp4?_nc_cat=111&_nc_sid=cf96c8&_nc_oc=AQmB2QMBA7d7Jbr6rbx099c3ekUdnJ4QAC6HFPYbeFT1xXaVHeTZ77FE87V-Y-0boKM&_nc_ht=video-lga3-1.xx&oh=f3eead2ef45a7462a06f4769999f72d7&oe=5EF1CFF2",
            "embedded_video_thumbnail_url": "https://scontent-lga3-1.xx.fbcdn.net/v/t39.16868-6/103577517_1636856286464501_3372490906273270732_n.jpg?_nc_cat=106&_nc_sid=cf96c8&_nc_oc=AQl9zCVeWfv8iAu2MuhRLh3T6WJ__iy78iVbwuPY-dlkQ34acLfifCcexjtOOk5OTbk&_nc_ht=scontent-lga3-1.xx&oh=6d50db37ca73726ebf62358080a863ed&oe=5F17710D",
            "product_url": "https://summerde.com/products/humidifier",
            "started_running": "2020-06-22T00:00:00+07:00",
            "saved": false,
            "price": 32.99,
            "created_at": "2020-06-22T14:16:17+07:00",
            "updated_at": "2020-06-22T14:16:19+07:00",
            "last_saved": "2020-06-23T13:55:16.395644+07:00",
            "description": "",
            "page_id": "462486437644644"
        }
    ]
}
```

If a server-side error occurs while processing the request, the response will include an error code along with a specific error message, for example:

```json
{
    "error": "Exception happened during processing of request from ('127.0.0.1', 51937)"
}
```

#### Insert or Update Facebook Ads Information

##### POST Endpoint
- **POST /api/v1/facebook-ads**

This endpoint allows you to insert or update all the information of Facebook Ads for a Facebook page received from scraping module in other VPS.

##### Example Request

```shell
curl -X POST "/api/v1/facebook-ads" -H "Content-Type: application/json" -d '{
    "facebook_page_id": "100252918194879",
    "facebook_page_url": "https://www.facebook.com/Aliands-100252918194879/",
    "facebook_page_name": "Aliands",
    "last_crawling": "2020-06-23 11:00:00.045345",
    "facebook_ads_info": {
        "269192497502903": {
            "advertising_id": "269192497502903",
            "started_running": "2020-06-22 00:00:00",
            "advertised_product_url": "https://www.aliands.com/products/steel-trimmer-head?variant=31644519497772",
            "embedded_video_url": "https://video-lga3-1.xx.fbcdn.net/v/t42.9040-2/99068041_1299815443542265_1456033311079006208_n.mp4?_nc_cat=110&_nc_sid=cf96c8&_nc_oc=AQkeMAGQNZC6RqQFS9Bu3OUYNdCmjAiSFVCQjffuK6luJMJRRMhzq9lz9LcKCabR63M&_nc_ht=video-lga3-1.xx&oh=88eb83bcea6fb7297ec5474c671a39cc&oe=5EF1A23E",
            "embedded_video_thumbnail_url": "https://scontent-lga3-1.xx.fbcdn.net/v/t39.16868-6/98186621_533238570685897_2242879432899428352_n.jpg?_nc_cat=100&_nc_sid=cf96c8&_nc_oc=AQlUBJaPhe9Y0Urb4pQj0Z27XlERj7J7gN-c93qrlcEUQxROzgsSBOec_NTuuZwMEcQ&_nc_ht=scontent-lga3-1.xx&oh=8b147e5d9745ee53780594069dc33de8&oe=5F16F388",
            "price": "36.99",
            "created_at": "2020-01-09 13:46:45",
            "updated_at": "2020-06-23 08:40:24"
        },
        "621273711928817": {
            "advertising_id": "621273711928817",
            "started_running": "2020-06-22 00:00:00",
            "advertised_product_url": "https://www.aliands.com/products/360-degree-car-dashboard-phone-holder?variant=34147072999468",
            "embedded_video_url": "https://video-lga3-1.xx.fbcdn.net/v/t42.1790-2/98021773_768801603652294_5810033012706363067_n.mp4?_nc_cat=100&_nc_sid=cf96c8&_nc_oc=AQnHA4hLTHT5d89xdPZ492lID9G3LalO0pnzfWxM5fU2VSwnTtrd7R-aRE2BUwdczjc&_nc_ht=video-lga3-1.xx&oh=25b274764ab6988635e41877e35976e8&oe=5EF19BED",
            "embedded_video_thumbnail_url": "https://scontent-lga3-1.xx.fbcdn.net/v/t39.16868-6/97974895_678093756376735_37680327908392960_n.jpg?_nc_cat=110&_nc_sid=cf96c8&_nc_oc=AQmNfCl1ROVEemLfeJDtb28ZeiZE8fH2I_D2_v7lYK0SXEHmadF-Q5OboT0M1lfDjWk&_nc_ht=scontent-lga3-1.xx&oh=d880807d131740352e7a7807fe098d79&oe=5F172C98",
            "price": "19.99",
            "created_at": "2020-05-15 16:12:26",
            "updated_at": "2020-06-23 08:45:36"
        }
    }
}'
```

This cURL request is made to the <i>POST /api/v1/facebook-ads</i> endpoint with the provided JSON data in the request body.

##### Example Response

If a server-side error occurs while processing the request, the response will include an error code along with a specific error message, for example:

```json
{
    "error": "IntegrityError: Insert or update on table \"facebook_ads_analyzer_facebook_ad\" violates foreign key constraint"
}
```

#### Update Facebook Ad Saved Status

##### PATCH Endpoint
- **PATCH /api/v1/facebook-ads**

This endpoint allows you to update the 'Saved' field value in the corresponding database table for Facebook Ads based on whether a note is checked or unchecked for each item in the interface.

##### Example Request

```bash
curl -X PATCH "/api/v1/facebook-ads" -H "Content-Type: application/json" -d '{
    "advertising_id": "280677846465210",
    "last_saved": "2020-06-23T19:53:54+07:00"
}'
```

In this example, a cURL request is made to the <i>PATCH /api/v1/facebook-ads</i> endpoint, which updates the 'Saved' field in the database table for the specified Facebook Ad based on the provided data.

##### Example Response

If a server-side error occurs while processing the request, the response will include an error code along with a specific error message. For example:

```json
{
    "error" : "String was not recognized as a valid DateTime"
}
```

The REST API system for functions related to collecting and analyzing data related to Facebook Video Posts is listed as follows:

#### Retrieve Basic Information of Facebook Video Posts

##### GET Endpoint
- **GET /api/v1/facebook-video-posts**

This endpoint allows you to retrieve a list of basic information for all video posts that users have uploaded to the tool and are stored in the database. Modules with data scraping functionality can send this request to receive a list of objects to collect data from.

##### Example Request

```bash
curl -X GET "/api/v1/facebook-video-posts"
```

##### Example Response

An example response for this request:

```json
[
    {
        "post_id": "661797551249763",
        "post_url": "https://www.facebook.com/100126171634519/videos/661797551249763/?t=4",
        "posted_at": "2020-03-26T16:00:00+07:00"
    },
    {
        "post_id": "735565840354184",
        "post_url": "https://www.facebook.com/100149401421864/videos/735565840354184/?t=4",
        "posted_at": "2020-08-14T01:22:00+07:00"
    },
    {
        "post_id": "1697513100386454",
        "post_url": "https://www.facebook.com/100192911776219/videos/1697513100386454/?t=5",
        "posted_at": "2020-07-16T00:41:00+07:00"
    },
    {
        "post_id": "323996228639704",
        "post_url": "https://www.facebook.com/100216448311350/videos/323996228639704/?t=17",
        "posted_at": "2020-07-21T20:39:00+07:00"
    },
    {
        "post_id": "2620586714872688",
        "post_url": "https://www.facebook.com/100227208399154/videos/2620586714872688/?t=6",
        "posted_at": "2020-06-07T10:07:00+07:00"
    },
    {
        "post_id": "3035338960047369",
        "post_url": "https://www.facebook.com/100249038417919/videos/3035338960047369/?t=10",
        "posted_at": "2020-07-02T00:12:00+07:00"
    },
    {
        "post_id": "629839814600551",
        "post_url": "https://www.facebook.com/100252918194879/videos/629839814600551/?t=3",
        "posted_at": "2020-08-13T01:29:00+07:00"
    },
    {
        "post_id": "308542443856195",
        "post_url": "https://www.facebook.com/100261445107950/videos/308542443856195/?t=23",
        "posted_at": "2020-07-17T01:04:00+07:00"
    }
]
```

In this example, a cURL request is made to the <i>GET /api/v1/facebook-video-posts</i> endpoint, which returns a JSON response containing basic information about the Facebook video posts in the database.

#### Insert or Update Facebook Video Posts

##### POST Endpoint
- **POST /api/v1/facebook-video-posts**

This endpoint is used to insert or update information for each Facebook Video Post that is scraped and sent from scraping module in other VPS.

##### Example Request

```bash
curl -X POST "/api/v1/facebook-video-posts" -d '{
    "post_url": "https://www.facebook.com/100149401421864/videos/735565840354184/?t=4",
    "last_crawling": "2020-08-31 04:00:00.072705",
    "post_id": "735565840354184",
    "post_status": true,
    "reaction_count": 10000,
    "share_count": 2000,
    "comment_count": 1500,
    "video_thumbnail_url": "https://scontent-lga3-2.xx.fbcdn.net/v/t15.5256-10/117921344_735565877020847_2443965757608234349_n.jpg?_nc_cat=1&_nc_sid=f2c4d5&_nc_ohc=_z1o3E_oOjEAX_1jaJ2&_nc_ht=scontent-lga3-2.xx&oh=8ce44659afd0b69faa971bfb6187cea2&oe=5F72CF64",
    "facebook_page_name": "Sunny Mode",
    "video_title": "You can trim even the hardest nails easily",
    "facebook_page_url": "https://www.facebook.com/100149401421864",
    "posted_at": "2020-08-14 01:22:00",
    "embedded_video_url": "https://scontent-lga3-2.xx.fbcdn.net/v/t66.36240-6/40150997_3260462127342280_992919829839726141_n.mp4?_nc_cat=101&_nc_sid=985c63&efg=eyJ2ZW5jb2RlX3rhZyI6Im9lcF9hZCJ9&_nc_ohc=X5XfMFZoe1UAX_ItXg1&_nc_ht=scontent-lga3-2.xx&oh=b535136d967025410e6478adace03380&oe=5F733163"
}'
```

In this example, a cURL request is made to the <i>POST /api/v1/facebook-video-posts</i> endpoint to insert or update information for a Facebook Video Post.

##### Example Response

If a server-side error occurs while processing the request, the response will include an error code along with a specific error message, for example:

```json
{
    "error": "django.db.utils.IntegrityError: duplicate key value violates unique constraint"
}
```

#### Video Posts Analytics

##### GET Endpoint
- **GET /api/v1/video-posts-analytics**

This endpoint allows you to query information for all video posts that have been collected and aggregated based on order and filter options. If the request does not include additional parameters, the server-side will use default parameters that are set up.

##### Example Request

This allows you to access a list of video posts with specific filter criteria:

```bash
curl -X "GET /api/v1/video-posts-analytics?tab=all-posts&period=all-time&sort_by=sharing&page=1"
```

- This request retrieves information about video posts with the following parameters:
  - `tab`: all-posts (to get all posted videos)
  - `period`: all-time (to retrieve data for all time)
  - `sort_by`: sharing (to order the display of post information based on the number of shares at the current time)
  - `page`: 1 (corresponding to loading the information of the first 50 posts)

##### Example Response

```json
{
    "count": 514,
    "next": "http://nguyentruonglong.com/api/v1/video-posts-analytics?page=2&period=all-time&sort_by=sharing&tab=all-posts",
    "previous": null,
    "results": [
        {
            "post_id": "738412159924000",
            "latest_reaction_count": 202000,
            "latest_share_count": 78000,
            "latest_comment_count": 34000,
            "post_url": "https://www.facebook.com/lumn8HQ/videos/738412159924000/?t=5",
            "post_status": true,
            "posted_at": "2019-08-14T11:14:00+07:00",
            "embedded_video_url": "https://video-lga3-2.xx.fbcdn.net/v/t39.24130-2/10000000_628942891058606_9214707785175600452_n.mp4?_nc_cat=108&_nc_sid=985c63&efg=eyJ2ZW5jb2RlX3RhZyI6Im9lcF9oZCJ9&_nc_ohc=1HUvV93o5AEAX-QCTBX&_nc_ht=video-lga3-2.xx&oh=9b9d14a22d1e28731176b4e6f52f664b&oe=5F733843",
            "video_thumbnail_url": "https://scontent-lga3-2.xx.fbcdn.net/v/t15.5256-10/66533145_738412266590656_5078960747787059200_n.jpg?_nc_cat=106&_nc_sid=f2c4d5&_nc_ohc=-r0jE-Js3L4AX-q7WTh&_nc_ht=scontent-lga3-2.xx&oh=52a3609f9d35fd8553ad5cdda33298d9&oe=5F70B899",
            "video_title": "Take The Worry Out Of Adding Accent Lighting Forever",
            "trending_index": 0,
            "last_crawling": "2020-08-31T04:00:00.072705+07:00",
            "facebook_page_url": "https://www.facebook.com/lumn8HQ",
            "facebook_page_name": "LUMN8",
            "saved": false,
            "last_saved": "2020-08-26T05:17:51.785351+07:00",
            "category": "Unknown Category",
            "description": "",
            "reaction_count_delta": 0,
            "share_count_delta": 0,
            "comment_count_delta": 0,
            "daily_engagement_chart": "{\"daily_reaction_count\": {\"1\": 0, \"2\": 201000, \"3\": 201000, \"4\": 201000, \"5\": 202000, \"6\": 202000, \"7\": 202000}, \"daily_share_count\": {\"1\": 0, \"2\": 78000, \"3\": 78000, \"4\": 78000, \"5\": 78000, \"6\": 78000, \"7\": 78000}, \"daily_comment_count\": {\"1\": 0, \"2\": 34000, \"3\": 34000, \"4\": 34000, \"5\": 34000, \"6\": 34000, \"7\": 34000}}"
        },
        {
            "post_id": "388088341742183",
            "latest_reaction_count": 227000,
            "latest_share_count": 51000,
            "latest_comment_count": 20000,
            "post_url": "https://www.facebook.com/mysecretwood/videos/388088341742183/?t=3",
            "post_status": true,
            "posted_at": "2019-02-23T01:42:00+07:00",
            "embedded_video_url": "https://scontent-lga3-2.xx.fbcdn.net/v/t66.36240-6/40249226_302122547549425_44168058639254796_n.mp4?_nc_cat=100&_nc_sid=985c63&efg=eyJ2ZW5jb2RlX3RhZyI6Im9lcF9oZCJ9&_nc_ohc=Xi_2qOXXPsMAX9anmPL&_nc_ht=scontent-lga3-2.xx&oh=235ac0d90e503f3c5ed71d744dc74ca3&oe=5F7138B4",
            "video_thumbnail_url": "https://scontent-lga3-2.xx.fbcdn.net/v/t15.5256-10/52112885_388088488408835_3793617219531833344_n.jpg?_nc_cat=110&_nc_sid=f2c4d5&_nc_ohc=3EFlxBbsbKQAX-qT6In&_nc_ht=scontent-lga3-2.xx&oh=e32f296f92465254d46300d8747ad1f7&oe=5F71ECC6",
            "video_title": "Related Videos",
            "trending_index": 0,
            "last_crawling": "2020-08-31T04:00:00.072705+07:00",
            "facebook_page_url": "https://www.facebook.com/mysecretwood",
            "facebook_page_name": "Secret Wood",
            "saved": false,
            "last_saved": "2020-08-26T05:19:11.478502+07:00",
            "category": "Unknown Category",
            "description": "",
            "reaction_count_delta": 0,
            "share_count_delta": 0,
            "comment_count_delta": 0,
            "daily_engagement_chart": "{\"daily_reaction_count\": {\"1\": 0, \"2\": 227000, \"3\": 227000, \"4\": 227000, \"5\": 227000, \"6\": 227000, \"7\": 227000}, \"daily_share_count\": {\"1\": 0, \"2\": 51000, \"3\": 51000, \"4\": 51000, \"5\": 51000, \"6\": 51000, \"7\": 51000}, \"daily_comment_count\": {\"1\": 0, \"2\": 20000, \"3\": 20000, \"4\": 20000, \"5\": 20000, \"6\": 20000, \"7\": 20000}}"
        },
        {
            "post_id": "191901468694975",
            "latest_reaction_count": 84000,
            "latest_share_count": 30000,
            "latest_comment_count": 17000,
            "post_url": "https://www.facebook.com/watch/?ref=saved&v=191901468694975",
            "post_status": true,
            "posted_at": "2020-02-18T21:56:00+07:00",
            "embedded_video_url": "https://video-lga3-2.xx.fbcdn.net/v/t39.24130-2/117837138_3172748659460714_6021706538825209849_n.mp4?_nc_cat=109&_nc_sid=985c63&efg=eyJ2ZW5jb2RlX3RhZyI6Im9lcF9oZCJ9&_nc_ohc=9gH3HBXyQswAX_n6bW-&_nc_ht=video-lga3-2.xx&oh=4ec68e06f7809c5014811e0547bebc41&oe=5F71487B",
            "video_thumbnail_url": "https://scontent-lga3-2.xx.fbcdn.net/v/t15.5256-10/81791779_191901522028303_5468957075482607616_n.jpg?_nc_cat=103&_nc_sid=f2c4d5&_nc_ohc=zTiWWKZLXUYAX_XXCyM&_nc_ht=scontent-lga3-2.xx&oh=be9cc0d0cf2930ef169c98770bdaba34&oe=5F701654",
            "video_title": "WIRELESS WIFI CAMERA WITH SENSORI NIGHT VISION",
            "trending_index": 0,
            "last_crawling": "2020-08-31T04:00:00.072705+07:00",
            "facebook_page_url": "https://www.facebook.com/100856337968554",
            "facebook_page_name": "Fogsale",
            "saved": false,
            "last_saved": "2020-08-19T05:04:14.244202+07:00",
            "category": "Unknown Category",
            "description": "",
            "reaction_count_delta": 0,
            "share_count_delta": 0,
            "comment_count_delta": 0,
            "daily_engagement_chart": "{\"daily_reaction_count\": {\"1\": 84000, \"2\": 84000, \"3\": 84000, \"4\": 84000, \"5\": 84000, \"6\": 84000, \"7\": 84000}, \"daily_share_count\": {\"1\": 30000, \"2\": 30000, \"3\": 30000, \"4\": 30000, \"5\": 30000, \"6\": 30000, \"7\": 30000}, \"daily_comment_count\": {\"1\": 17000, \"2\": 17000, \"3\": 17000, \"4\": 17000, \"5\": 17000, \"6\": 17000, \"7\": 17000}}"
        }
    ]
}
```

#### Update Video Post Saved Status

##### PATCH Endpoint
- **PATCH /api/v1/video-posts-analytics**

This request is used to update the value of the 'Saved' field in the corresponding database table for a video post when a user interacts with the tool by marking or unmarking it with a heart-shaped icon on each item in the interface.

##### Example Request
  
```shell
curl -X "PATCH /api/v1/video-posts-analytics" -H "Content-Type: application/json" -d '{
    "post_id": "2026135554197150",
    "last_saved": "2020-08-31T21:43:06+07:00"
}
```

This example demonstrates how to structure the request body to update the saved status of a video post.

##### Example Response

In case of a server-side error during request processing, the response will include an error code and a specific error message. For instance:

```json
{
    "error": "String was not recognized as a valid DateTime"
}
```

#### Upload Data to Server

##### POST Endpoint
- **POST /api/v1/upload**

This request is used to perform data upload tasks, allowing users to upload files like text files or CSV files to the server-side.

##### Example Request

```shell
curl -X POST "/api/v1/upload" -F "file=@/Users/Username/Desktop/data.csv"
```

This cURL request will upload the specified file to the <i>POST /api/v1/upload</i> endpoint of the server-side.

##### Example Response

If a server-side error occurs during the processing of the upload request, the response will include an error code and a specific error message. Here's an example of such an error:

```json
{
    "error": "MemoryError: memory allocation failed"
}
```


### Demo of Some Key Features

<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 005.png">
        <figcaption style="text-align:center"><i>Shopify Stores Tracker Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 018.png">
        <figcaption style="text-align:center"><i>Best Selling Products Tracker Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 025.png">
        <figcaption style="text-align:center"><i>Facebook Posts Tracker Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 014.png">
        <figcaption style="text-align:center"><i>Facebook Ads Tracker Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 006.png">
        <figcaption style="text-align:center"><i>Shopify Data Manager Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 032.png">
        <figcaption style="text-align:center"><i>Facebook Post Data Manager Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 030.png">
        <figcaption style="text-align:center"><i>Facebook Page Data Manager Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 008.png">
        <figcaption style="text-align:center"><i>Facebook Page Data Manager Interface</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="docs/images/Full Page Screenshot 021.png">
        <figcaption style="text-align:center"><i>Data Upload Interface</i></figcaption>
    </center>
</figure>
